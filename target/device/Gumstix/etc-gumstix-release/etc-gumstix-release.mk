#############################################################
#
# /etc/gumstix_release file
#
# Maintainer: Albert den Haan <albert.denhaan@sympatico.ca>
#
#############################################################
# Copyright (C) 2005 by Albert den Haan <albert.denhaan@sympatico.ca>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Library General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
# USA

# TARGETS
TARGET_FILENAME=$(TARGET_DIR)/etc/gumstix-release

etc-gumstix-release-clean: 


etc-gumstix-release-dirclean: 


etc-gumstix-release: 
	echo DISTRIB_ID=\'gumstix\' > $(TARGET_FILENAME)
	echo DISTRIB_DESCRIPTION=\'\' >> $(TARGET_FILENAME)
	echo DISTRIB_RELEASE=\'`svnversion $(BASE_DIR)`\' >> $(TARGET_FILENAME)
	echo DISTRIB_CODENAME=\'\' >> $(TARGET_FILENAME)
	echo BUILD_DATE=\'`date`\' >> $(TARGET_FILENAME)
	echo BUILD_HOSTNAME=\'`hostname -f`\' >> $(TARGET_FILENAME)
