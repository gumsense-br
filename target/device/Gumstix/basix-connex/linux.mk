#############################################################
#
# Linux kernel targets
#
# Note:  If you have any patches to apply, create the directory
# sources/kernel-patches and put your patches in there and number
# them in the order you wish to apply them...  i.e.
#
#   sources/kernel-patches/001-my-special-stuff.bz2
#   sources/kernel-patches/003-gcc-Os.bz2
#   sources/kernel-patches/004_no-warnings.bz2
#   sources/kernel-patches/030-lowlatency-mini.bz2
#   sources/kernel-patches/031-lowlatency-fixes-5.bz2
#   sources/kernel-patches/099-shutup.bz2
#   etc...
#
# these patches will all be applied by the patch-kernel.sh
# script (which will also abort the build if it finds rejects)
#  -Erik
#
#############################################################
ifneq ($(filter $(TARGETS),linux),)

# Version of Linux to download and then apply patches to
DOWNLOAD_LINUX_VERSION=2.6.20
# Version of Linux AFTER patches
LINUX_VERSION=$(DOWNLOAD_LINUX_VERSION)gum

LINUX_FORMAT=compressed/vmlinux
#LINUX_FORMAT=images/zImage.prep
LINUX_KARCH:=$(shell echo $(ARCH) | sed -e 's/i[3-9]86/i386/' \
	-e 's/mipsel/mips/' \
	-e 's/powerpc/ppc/' \
	-e 's/sh[234]/sh/' \
	)
LINUX_BINLOC=arch/$(LINUX_KARCH)/boot/$(LINUX_FORMAT)

LINUX_DIR=$(BUILD_DIR)/linux-$(LINUX_VERSION)
LINUX_HEADERS_DIR=$(LINUX_DIR)
LINUX_SOURCE=linux-$(DOWNLOAD_LINUX_VERSION).tar.bz2
LINUX_SITE=http://www.kernel.org/pub/linux/kernel/v2.6
LINUX_KCONFIG=$(BASE_DIR)/target/device/Gumstix/basix-connex/linux.config
LINUX_PATCH_DIR=$(BASE_DIR)/target/device/Gumstix/basix-connex/kernel-patches
LINUX_KERNEL=$(TARGET_DIR)/boot/uImage
# Used by pcmcia-cs and others
LINUX_SOURCE_DIR=$(LINUX_DIR)
LINUX_UIMAGE=arch/arm/boot/uImage
pathsearch = $(firstword $(wildcard $(addsuffix /$(1),$(subst :, ,/sbin))))
DEPMOD_TMP := $(call pathsearch,depmod)
ifneq "$(DEPMOD_TMP)" "/sbin/depmod"
DEPMOD=$(BUSYBOX_DIR)/examples/depmod.pl
echo DEPMOD1=$(DEPMOD)
else
DEPMOD=/sbin/depmod
echo DEPMOD2=$(DEPMOD)
endif
#ifneq  ($(DEPMOD),/sbin/depmod)
#echo No depmod on path, using busybox variant
#DEPMOD=$(BUSYBOX_DIR)/examples/depmod.pl
#endif

$(DL_DIR)/$(LINUX_SOURCE):
	-mkdir -p $(DL_DIR)
	$(WGET) -P $(DL_DIR) $(LINUX_SITE)/$(LINUX_SOURCE)

$(LINUX_DIR)/Makefile: $(DL_DIR)/$(LINUX_SOURCE) $(MTD_DIR)/Makefile 
	-mkdir -p $(TOOL_BUILD_DIR)
	#mkdir -p $(LINUX_DIR)
	#rm -rf $(LINUX_DIR)
	-echo DEPMOD3=$(DEPMOD)
	-echo DEPMOD_TMP=$(DEPMOD_TMP)
	bzcat $(DL_DIR)/$(LINUX_SOURCE) | tar -C $(BUILD_DIR) -xf -
ifneq ($(DOWNLOAD_LINUX_VERSION),$(LINUX_VERSION))
	# Rename the dir from the downloaded version to the AFTER patch version	
	mv -f $(BUILD_DIR)/linux-$(DOWNLOAD_LINUX_VERSION) $(BUILD_DIR)/linux-$(LINUX_VERSION)
endif
	# Insert latest MTD code to kernel tree
	#(cd $(MTD_DIR); sh patches/patchin.sh -c -2 -y $(LINUX_DIR))
	-mkdir -p $(LINUX_PATCH_DIR)
	(cd $(LINUX_DIR); QUILT_PATCHES=$(LINUX_PATCH_DIR) $(QUILT) push -a)
	echo $(LINUX_PATCH_DIR) > $(LINUX_DIR)/.patched
	# Check if depmod is right version, and if not, apply depmod patch
ifneq "$(DEPMOD)" "/sbin/depmod"
	$(SOURCE_DIR)/patch-kernel.sh $(LINUX_DIR) $(SOURCE_DIR) linux-depmod.patch
	echo Patched depmod usage
endif
	echo DEPMOD now set to $(DEPMOD)
	-(cd $(TOOL_BUILD_DIR); rm -f linux; ln -sf $(LINUX_DIR) linux)
	touch $@

$(LINUX_KCONFIG):
	@if [ ! -f "$(LINUX_KCONFIG)" ] ; then \
		echo ""; \
		echo "You should create a .config for your kernel"; \
		echo "and install it as $(LINUX_KCONFIG)"; \
		echo ""; \
		sleep 5; \
	fi;

$(LINUX_DIR)/include/linux/autoconf.h $(BUILD_DIR)/linux/include/linux/autoconf.h:  $(LINUX_DIR)/Makefile  $(LINUX_KCONFIG) 
	-cp $(LINUX_KCONFIG) $(LINUX_DIR)/.config
ifeq ($(BR2_ARM_EABI),y)
	$(SED) 's,# CONFIG_AEABI is not set,CONFIG_AEABI=y,g' $(LINUX_DIR)/.config
endif
	$(MAKE) $(JLEVEL) -C $(LINUX_DIR) CROSS_COMPILE=$(KERNEL_CROSS) ARCH=$(ARCH) silentoldconfig
	$(MAKE) $(JLEVEL) -C $(LINUX_DIR) CROSS_COMPILE=$(KERNEL_CROSS) ARCH=$(ARCH) prepare1 include/asm-arm/.arch

$(LINUX_DIR)/$(LINUX_UIMAGE): $(LINUX_DIR)/$(LINUX_BINLOC) $(STAGING_DIR)/bin/mkimage
	$(KERNEL_CROSS)objcopy -O binary -R .note -R .comment -S $(LINUX_DIR)/$(LINUX_BINLOC) $(LINUX_DIR)/linux.bin
	$(STAGING_DIR)/bin/mkimage -A arm -O linux -T kernel -C none -a 0xa0008000 -e 0xa0008000 -n "uImage" -d $(LINUX_DIR)/linux.bin $(LINUX_DIR)/$(LINUX_UIMAGE)

$(LINUX_DIR)/$(LINUX_BINLOC): $(LINUX_DIR)/include/linux/autoconf.h
	$(MAKE) $(JLEVEL) -C $(LINUX_DIR) CROSS_COMPILE=$(KERNEL_CROSS) ARCH=$(ARCH)

$(LINUX_KERNEL): $(LINUX_DIR)/$(LINUX_UIMAGE) $(BUSYBOX_DIR)/.configured
	$(MAKE) $(JLEVEL) -C $(LINUX_DIR) DEPMOD="$(DEPMOD)" CROSS_COMPILE=$(KERNEL_CROSS) ARCH=$(ARCH) INSTALL_MOD_PATH=$(TARGET_DIR) modules_install
	find $(TARGET_DIR)/lib/modules -type l -name build -exec rm {} \;
	find $(TARGET_DIR)/lib/modules -type l -name source -exec rm {} \;
	mkdir -p $(TARGET_DIR)/boot
	cp -fpR --no-dereference $(LINUX_DIR)/$(LINUX_UIMAGE) $(LINUX_KERNEL)
	(grep -q pxa2xx-cs $(TARGET_DIR)/etc/modprobe.conf || \
	 echo -e '\ninstall pcmcia /sbin/modprobe --ignore-install pcmcia && modprobe pxa2xx-cs\n' >> $(TARGET_DIR)/etc/modprobe.conf)
	touch -c $(LINUX_KERNEL)

$(STAGING_DIR)/include/linux/version.h: $(LINUX_DIR)/include/linux/autoconf.h $(LINUX_DIR)/include/linux/version.h
	mkdir -p $(STAGING_DIR)/include
	tar -ch -C $(LINUX_DIR)/include -f - linux | tar -xf - -C $(STAGING_DIR)/include/
	tar -ch -C $(LINUX_DIR)/include -f - asm | tar -xf - -C $(STAGING_DIR)/include/
	touch $(STAGING_DIR)/include/linux/version.h

kernel-headers: $(STAGING_DIR)/include/linux/version.h

linux: kernel-headers $(LINUX_KERNEL)

linux-source: $(DL_DIR)/$(LINUX_SOURCE)

# This has been renamed so we do _NOT_ by default run this on 'make clean'
linuxclean: clean
	rm -f $(LINUX_KERNEL) $(LINUX_DIR)/$(LINUX_UIMAGE)
	-$(MAKE) $(JLEVEL) -C $(LINUX_DIR) CROSS_COMPILE=$(KERNEL_CROSS) ARCH=$(ARCH) clean

linux-dirclean:
	rm -rf $(LINUX_DIR)

endif
