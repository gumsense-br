#############################################################
#
# u-boot
#
#############################################################

UBOOT_VERSION=1.1.4

UBOOT_SOURCE=u-boot-$(UBOOT_VERSION).tar.bz2
UBOOT_SITE=ftp://ftp.denx.de/pub/u-boot
UBOOT_DIR=$(BUILD_DIR)/${shell basename $(UBOOT_SOURCE) .tar.bz2}

$(DL_DIR)/$(UBOOT_SOURCE):
	$(WGET) -P $(DL_DIR) $(UBOOT_SITE)/$(UBOOT_SOURCE)

$(UBOOT_DIR)/.unpacked:	$(DL_DIR)/$(UBOOT_SOURCE)
	bzcat $(DL_DIR)/$(UBOOT_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	touch $(UBOOT_DIR)/.unpacked

$(UBOOT_DIR)/.patched: $(UBOOT_DIR)/.unpacked
	(cd $(UBOOT_DIR); QUILT_PATCHES=$(BASE_DIR)/target/arm/u-boot $(QUILT) push -a)
	echo $(BASE_DIR)/target/arm/u-boot > $(UBOOT_DIR)/.patched

$(UBOOT_DIR)/.configured: $(UBOOT_DIR)/.patched
	$(MAKE) $(JLEVEL) ARCH=$(ARCH) CROSS_COMPILE=$(KERNEL_CROSS) -C $(UBOOT_DIR) gumstix_config
	touch $(UBOOT_DIR)/.configured

$(UBOOT_DIR)/u-boot.bin: $(UBOOT_DIR)/.configured
	$(MAKE) $(JLEVEL) GUMSTIX_400MHZ=$(GUMSTIX_400MHZ) SVN_REVISION=$(shell svnversion) ARCH=$(ARCH) CROSS_COMPILE=$(KERNEL_CROSS) -C $(UBOOT_DIR)

$(TARGET_DIR)/usr/sbin/fw_printenv: $(UBOOT_DIR)/.configured $(UBOOT_DIR)/lib_generic/crc32.c $(UBOOT_DIR)/tools/env/fw_env.c $(UBOOT_DIR)/tools/env/fw_env_main.c $(UBOOT_DIR)/tools/env/fw_env.h
	$(TARGET_CC) -I$(UBOOT_DIR)/include -I$(LINUX_DIR)/include $(UBOOT_DIR)/lib_generic/crc32.c $(UBOOT_DIR)/tools/env/fw_env.c $(UBOOT_DIR)/tools/env/fw_env_main.c $(TARGET_OPTIMIZATION) -o $@
	$(STRIP) $@
	rm -f $(TARGET_DIR)/usr/sbin/fw_setenv
	ln -s fw_printenv $(TARGET_DIR)/usr/sbin/fw_setenv

$(BASE_DIR)/u-boot.bin: $(UBOOT_DIR)/u-boot.bin
	cp -f $(UBOOT_DIR)/u-boot.bin $(BASE_DIR)
	cp -f $(UBOOT_DIR)/u-boot.srec $(BASE_DIR)

fw_printenv: $(TARGET_DIR)/usr/sbin/fw_printenv

u-boot: $(BASE_DIR)/u-boot.bin

$(STAGING_DIR)/bin/mkimage: $(UBOOT_DIR)/.configured
	$(MAKE) $(JLEVEL) ARCH=$(ARCH) CROSS_COMPILE=$(KERNEL_CROSS) -C $(UBOOT_DIR) tools
	cp -f $(UBOOT_DIR)/tools/mkimage $(STAGING_DIR)/bin/mkimage

u-boot-source: $(DL_DIR)/$(UBOOT_SOURCE)

u-boot-clean:
	@if [ -d $(UBOOT_DIR)/Makefile ] ; then \
		$(MAKE) $(JLEVEL) ARCH=$(ARCH) CROSS_COMPILE=$(KERNEL_CROSS) -C $(UBOOT_DIR) clean ; \
	fi;

u-boot-dirclean:
	rm -rf $(UBOOT_DIR)

ifeq ($(strip $(BR2_TARGET_UBOOT)),y)
TARGETS+=u-boot
endif

ifeq ($(strip $(BR2_TARGET_UBOOT_FWPRINT)),y)
TARGETS+=fw_printenv
endif
