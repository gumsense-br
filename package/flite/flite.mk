#############################################################
#
# flite
#
#############################################################
FLITE_SOURCE:=flite-1.3-release.tar.gz
FLITE_SITE:=http://www.speech.cs.cmu.edu/flite/packed/flite-1.3
FLITE_CAT:=zcat
FLITE_DIR:=$(BUILD_DIR)/flite-1.3-release
FLITE_BINARY:=bin/flite
FLITE_TARGET_BINARY:=usr/bin/flite

FLITE_VOX:=cmu_us_kal
FLITE_LANG:=usenglish
FLITE_LEX:=cmulex
FLITE_AUDIO:=oss

$(DL_DIR)/$(FLITE_SOURCE):
	 $(WGET) -P $(DL_DIR) $(FLITE_SITE)/$(FLITE_SOURCE)

flite-source: $(DL_DIR)/$(FLITE_SOURCE)

$(FLITE_DIR)/.unpacked: $(DL_DIR)/$(FLITE_SOURCE)
	$(FLITE_CAT) $(DL_DIR)/$(FLITE_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	toolchain/patch-kernel.sh $(FLITE_DIR) package/flite flite*.patch
	touch $(FLITE_DIR)/.unpacked

$(FLITE_DIR)/.configured: $(FLITE_DIR)/.unpacked
	(cd $(FLITE_DIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) CC_FOR_BUILD=$(HOSTCC) \
		CFLAGS="$(TARGET_CFLAGS)" \
		ac_cv_func_setvbuf_reversed=no \
		flite_cv_have_mbstate_t=yes \
		./configure \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=/usr \
		--exec-prefix=/usr \
		--bindir=/usr/bin \
		--sbindir=/usr/sbin \
		--libexecdir=/usr/lib \
		--sysconfdir=/etc \
		--datadir=/usr/share \
		--localstatedir=/var \
		--mandir=/usr/man \
		--infodir=/usr/info \
		--disable-shared \
		--with-audio=$(FLITE_AUDIO) \
		--with-lang=$(FLITE_LANG) \
		--with-vox=$(FLITE_VOX) \
		--with-lex=$(FLITE_LEX) \
	);
	$(SED) 's/const unsigned/unsigned/' $(FLITE_DIR)/lang/cmu_us_kal*/cmu_us_kal*_diphone.c $(FLITE_DIR)/lang/cmu_time_awb/cmu_time_awb_lpc.c $(FLITE_DIR)/lang/cmu_time_awb/cmu_time_awb_mcep.c
	touch  $(FLITE_DIR)/.configured

$(FLITE_DIR)/$(FLITE_BINARY): $(FLITE_DIR)/.configured
	$(MAKE) $(JLEVEL) -C $(FLITE_DIR)

$(TARGET_DIR)/$(FLITE_TARGET_BINARY): $(FLITE_DIR)/$(FLITE_BINARY)
	install -m 0755 -D $< $@
	$(STRIP) $@

flite: uclibc $(TARGET_DIR)/$(FLITE_TARGET_BINARY)

flite-clean:
	rm $(TARGET_DIR)/$(FLITE_TARGET_BINARY)
	-$(MAKE) -C $(FLITE_DIR) clean

flite-dirclean:
	rm -rf $(FLITE_DIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_FLITE)),y)
TARGETS+=flite
endif
