#############################################################
#
# jamvm
#
#############################################################
JAMVM_SOURCE:=jamvm-1.4.2.tar.gz
JAMVM_SITE:=http://$(BR2_SOURCEFORGE_MIRROR).dl.sourceforge.net/sourceforge/jamvm
JAMVM_CAT:=zcat
JAMVM_DIR:=$(BUILD_DIR)/jamvm-1.4.2

JAMVM_BINARY=src/jamvm
JAMVM_TARGET_BINARY:=usr/bin/jamvm

$(DL_DIR)/$(JAMVM_SOURCE):
	 $(WGET) -P $(DL_DIR) $(JAMVM_SITE)/$(JAMVM_SOURCE)

classpath-source: $(DL_DIR)/$(JAMVM_SOURCE)

$(JAMVM_DIR)/.unpacked: $(DL_DIR)/$(JAMVM_SOURCE)
	$(JAMVM_CAT) $(DL_DIR)/$(JAMVM_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	touch $(JAMVM_DIR)/.unpacked

$(JAMVM_DIR)/.configured: $(JAMVM_DIR)/.unpacked
	(cd $(JAMVM_DIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) CC_FOR_BUILD=$(HOSTCC) \
		CFLAGS="$(TARGET_CFLAGS)" \
		./configure \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=/usr \
		--exec-prefix=/usr \
		--bindir=/usr/bin \
		--sbindir=/usr/sbin \
		--libexecdir=/usr/lib \
		--sysconfdir=/etc \
		--datadir=/usr/share \
		--localstatedir=/var \
		--mandir=/usr/man \
		--infodir=/usr/info \
		--with-classpath-install-dir=/usr \
	);
	touch  $(JAMVM_DIR)/.configured


$(JAMVM_DIR)/$(JAMVM_BINARY): $(JAMVM_DIR)/.configured
	$(MAKE) CC=$(TARGET_CC) CC_FOR_BUILD=$(HOSTCC) -C $(JAMVM_DIR)

$(TARGET_DIR)/$(JAMVM_TARGET_BINARY): $(JAMVM_DIR)/$(JAMVM_BINARY)
	$(MAKE) DESTDIR=$(TARGET_DIR) CC=$(TARGET_CC) -C $(JAMVM_DIR) install

jamvm: uclibc zlib $(TARGET_DIR)/$(JAMVM_TARGET_BINARY)
#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_JAMVM)),y)
TARGETS+=jamvm
endif
