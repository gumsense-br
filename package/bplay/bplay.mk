#############################################################
#
# bplay/brec
#
#############################################################
BPLAY_SOURCE=bplay-0.991.tar.gz
BPLAY_SITE:=http://www.amberdata.demon.co.uk/bplay
BPLAY_DIR=$(BUILD_DIR)/bplay-0.991


$(DL_DIR)/$(BPLAY_SOURCE):
	$(WGET) -P $(DL_DIR) $(BPLAY_SITE)/$(BPLAY_SOURCE)

$(BPLAY_DIR): $(DL_DIR)/$(BPLAY_SOURCE)
	zcat $(DL_DIR)/$(BPLAY_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -

$(BPLAY_DIR)/bplay: $(BPLAY_DIR)
	$(MAKE) CC=$(TARGET_CROSS)gcc CFLAGS="$(TARGET_CFLAGS) -DUSEBUFFLOCK" -C $(BPLAY_DIR);
	-$(STRIP) $(BPLAY_DIR)/bplay;

$(TARGET_DIR)/usr/bin/bplay: $(BPLAY_DIR)/bplay
	cp -P $(BPLAY_DIR)/bplay $(BPLAY_DIR)/brec $(TARGET_DIR)/usr/bin

bplay: uclibc $(TARGET_DIR)/usr/bin/bplay

bplay-source: $(DL_DIR)/$(BPLAY_SOURCE)

bplay-clean:
	rm -f $(TARGET_DIR)/usr/bin/bplay $(TARGET_DIR)/usr/bin/brec
	-$(MAKE) -C $(BPLAY_DIR) clean

bplay-dirclean:
	rm -rf $(BPLAY_DIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_BPLAY)),y)
TARGETS+=bplay
endif
