#############################################################
#
# player
#
#############################################################
PLAYER_SOURCE:=player-1.6.2.tar.gz
PLAYER_SITE:=http://$(BR2_SOURCEFORGE_MIRROR).dl.sourceforge.net/sourceforge/playerstage/

PLAYER_DIR:=$(BUILD_DIR)/player-1.6.2
PLAYER_CAT:=zcat
PLAYER_BINARY:=player
PLAYER_TARGET_BINARY:=usr/bin/player

$(DL_DIR)/$(PLAYER_SOURCE):
	$(WGET) -P $(DL_DIR) $(PLAYER_SITE)/$(PLAYER_SOURCE)

player-source: $(DL_DIR)/$(PLAYER_SOURCE)

$(PLAYER_DIR)/.unpacked: $(DL_DIR)/$(PLAYER_SOURCE)
	$(PLAYER_CAT) $(DL_DIR)/$(PLAYER_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	toolchain/patch-kernel.sh $(PLAYER_DIR) package/player player\*.patch
	touch $(PLAYER_DIR)/.unpacked

$(PLAYER_DIR)/.configured: $(PLAYER_DIR)/.unpacked
	(cd $(PLAYER_DIR); rm -rf config.cache; \
	 $(TARGET_CONFIGURE_OPTS) \
	 CFLAGS="$(TARGET_CFLAGS)" \
	 ./configure \
	 --without-rtk \
	 --disable-acoustics \
	 --disable-acts	\
	 --disable-amcl	\
	 --disable-amtecpowercube \
	 --disable-bumpersafe \
	 --disable-camera1394 \
	 --disable-cameracompress \
	 --disable-camerav4l \
	 --disable-cannonvcc4 \
	 --disable-cmucam2 \
	 --disable-cmvision \
	 --disable-dummy \
	 --disable-festival \
	 --enable-garminnmea \
	 --disable-gazebo \
	 --disable-highspeedsick \
	 --disable-imageseq \
	 --disable-isense \
	 --disable-iwspy \
	 --disable-khepera \
	 --disable-laserbar \
	 --disable-laserbarcode \
	 --disable-lasercspace \
	 --disable-laservisualbarcode \
	 --disable-laservisualbw \
	 --disable-libplayerc-py \
	 --disable-lifomcom \
	 --disable-linuxjoystick \
	 --disable-linuxwifi \
	 --disable-logfile \
	 --disable-mapcspace \
	 --disable-mapfile \
	 --disable-mapscale \
	 --disable-microstrain \
	 --disable-mixer \
	 --disable-obot \
	 --disable-p2os	\
	 --disable-passthrough \
	 --disable-playerclient_thread \
	 --disable-ptu46 \
	 --disable-reb \
	 --disable-rflex \
	 --disable-segwayrmp \
	 --disable-service_adv_lsd \
	 --disable-service_adv_mdns \
	 --disable-sicklms200 \
	 --disable-sickpls \
	 --disable-simpleshape \
	 --disable-sonyevid30 \
	 --enable-stage \
	 --disable-tests \
	 --disable-upcbarcode \
	 --disable-vfh \
	 --disable-waveaudio \
	 --disable-wavefront \
	 --disable-clodbuster \
	 --disable-er1 \
	 --disable-fixedtones \
	 --disable-flockofbirds\
	 --target=$(GNU_TARGET_NAME) \
	 --host=$(GNU_TARGET_NAME) \
	 --build=$(GNU_HOST_NAME) \
	 --prefix=/usr \
	 --exec-prefix=/usr \
	 --bindir=/usr/bin \
	 --sbindir=/usr/sbin \
	 --libexecdir=/usr/lib \
	 --sysconfdir=/etc \
	 --datadir=/usr/share \
	 --localstatedir=/var \
	 --mandir=/usr/man \
	 --infodir=/usr/info \
	 $(DISABLE_NLS) \
	 $(DISABLE_LARGEFILE) \
	);
	touch  $(PLAYER_DIR)/.configured

$(PLAYER_DIR)/$(PLAYER_BINARY): $(PLAYER_DIR)/.configured
	$(MAKE) -C $(PLAYER_DIR)

$(TARGET_DIR)/$(PLAYER_TARGET_BINARY): $(PLAYER_DIR)/$(PLAYER_BINARY)
	$(MAKE) DESTDIR=$(TARGET_DIR) -C $(PLAYER_DIR) install

player: uclibc jpeg $(TARGET_DIR)/$(PLAYER_TARGET_BINARY)

player-clean:
	$(MAKE) DESTDIR=$(TARGET_DIR) -C $(PLAYER_DIR) uninstall
	-$(MAKE) -C $(PLAYER_DIR) clean

player-dirclean:
	rm -rf $(PLAYER_DIR) 

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_PLAYER)),y)
TARGETS+=player
endif
