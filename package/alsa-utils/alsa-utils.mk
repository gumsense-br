#############################################################
#
# ALSA utils
#
#############################################################
ALSA_UTILS_VERSION:=1.0.11rc2
ALSA_UTILS_SOURCE:=alsa-utils-$(ALSA_UTILS_VERSION).tar.bz2
ALSA_UTILS_SITE:=ftp://ftp.alsa-project.org/pub/utils
ALSA_UTILS_DIR:=$(BUILD_DIR)/${shell basename $(ALSA_UTILS_SOURCE) .tar.bz2}
ALSA_UTILS_WORKDIR:=$(BUILD_DIR)/alsa-utils-$(ALSA_UTILS_VERSION)
ALSA_UTILS_BINARY:=aplay

# TODO: Not necessary for new buildroot?
TAR_OPTIONS:=$(TAR_OPTIONS)

$(DL_DIR)/$(ALSA_UTILS_SOURCE):
	$(WGET) -P $(DL_DIR) $(ALSA_UTILS_SITE)/$(ALSA_UTILS_SOURCE)

$(ALSA_UTILS_DIR)/.unpacked: $(DL_DIR)/$(ALSA_UTILS_SOURCE)
	bzcat $(DL_DIR)/$(ALSA_UTILS_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	toolchain/patch-kernel.sh $(ALSA_UTILS_DIR) package/alsa-utils alsa-utils*.patch*
	touch $(ALSA_UTILS_DIR)/.unpacked

# TODO: Check if ncurses is available -- if so, build alsamixer.
$(ALSA_UTILS_DIR)/.configured: $(ALSA_UTILS_DIR)/.unpacked
	(cd $(ALSA_UTILS_DIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) \
		CFLAGS="$(TARGET_CFLAGS)" \
		./configure \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--with-softfloat \
		--disable-alsatest \
		--disable-alsamixer \
		--with-alsa-prefix=$(STAGING_DIR)/lib \
		--with-alsa-inc-prefix=$(STAGING_DIR)/include \
		$(DISABLE_NLS) \
		$(DISABLE_LARGEFILE) \
	);
	touch $(ALSA_UTILS_DIR)/.configured

$(ALSA_UTILS_DIR)/$(ALSA_UTILS_BINARY)/$(ALSA_UTILS_BINARY): $(ALSA_UTILS_DIR)/.configured
	$(MAKE) CC=$(TARGET_CC) -C $(ALSA_UTILS_WORKDIR)

# TODO: Only one target -- is that bad?
$(TARGET_DIR)/usr/bin/$(ALSA_UTILS_BINARY): $(ALSA_UTILS_DIR)/$(ALSA_UTILS_BINARY)/$(ALSA_UTILS_BINARY)
	for i in aplay amixer;do \
		install -D -m 0755 $(ALSA_UTILS_DIR)/$$i/$$i $(TARGET_DIR)/usr/bin/$$i; \
		$(STRIP) $(TARGET_DIR)/usr/bin/$$i; \
	done
	(cd $(TARGET_DIR)/usr/bin;rm -f arecord;ln -s aplay arecord)

alsa-utils: alsa-lib $(TARGET_DIR)/usr/bin/$(ALSA_UTILS_BINARY)

alsa-utils-clean:
	$(MAKE) -C $(ALSA_UTILS_DIR) distclean
	rm $(ALSA_UTILS_DIR)/.configured

alsa-utils-dirclean:
	rm -rf $(ALSA_UTILS_DIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_ALSA_UTILS)),y)
TARGETS+=alsa-utils
endif

