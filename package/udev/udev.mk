#############################################################
#
# udev
#
#############################################################
UDEV_VERSION:=101
UDEV_SOURCE:=udev-$(UDEV_VERSION).tar.bz2
UDEV_SITE:=ftp://ftp.kernel.org/pub/linux/utils/kernel/hotplug/
UDEV_CAT:=bzcat
UDEV_DIR:=$(BUILD_DIR)/udev-$(UDEV_VERSION)
UDEV_TARGET_BINARY:=sbin/udev
UDEV_BINARY:=udev

# UDEV_ROOT is /dev so we can replace devfs, not /udev for experiments
UDEV_ROOT:=/dev

$(DL_DIR)/$(UDEV_SOURCE):
	 $(WGET) -P $(DL_DIR) $(UDEV_SITE)/$(UDEV_SOURCE)

udev-source: $(DL_DIR)/$(UDEV_SOURCE)

$(UDEV_DIR)/.unpacked: $(DL_DIR)/$(UDEV_SOURCE)
	$(UDEV_CAT) $(DL_DIR)/$(UDEV_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	toolchain/patch-kernel.sh $(UDEV_DIR) package/udev udev-*.patch
	touch $(UDEV_DIR)/.unpacked

$(UDEV_DIR)/.configured: $(UDEV_DIR)/.unpacked
	touch $(UDEV_DIR)/.configured

$(UDEV_DIR)/$(UDEV_BINARY): $(UDEV_DIR)/.configured
	$(MAKE) EXTRAS=extras/firmware CROSS_COMPILE="$(TARGET_CROSS)" \
		CFLAGS="${TARGET_CFLAGS} -D_GNU_SOURCE -D_FILE_OFFSET_BITS=64" \
		USE_LOG=false USE_SELINUX=false \
		udevdir=$(UDEV_ROOT) -C $(UDEV_DIR)
	touch -c $(UDEV_DIR)/$(UDEV_BINARY)

# UDEV_CONF overrides default policies for device access control and naming;
# default access controls prevent non-root tasks from running.  Many of the
# rule files rely on PROGRAM invocations (e.g. extra /etc/udev/scripts);
# for now we'll avoid having buildroot systems rely on them.
UDEV_CONF:=etc/udev/frugalware/*

$(TARGET_DIR)/$(UDEV_TARGET_BINARY): $(UDEV_DIR)/$(UDEV_BINARY)
	-mkdir -p $(TARGET_DIR)/sys
	mkdir -p $(TARGET_DIR)/etc/udev/rules.d
	install -m 0644 $(UDEV_DIR)/$(UDEV_CONF) $(TARGET_DIR)/etc/udev/rules.d
	$(MAKE) EXTRAS=extras/firmware CROSS_COMPILE="$(TARGET_CROSS)" DESTDIR="$(TARGET_DIR)" \
		CFLAGS="${TARGET_CFLAGS} -D_GNU_SOURCE -D_FILE_OFFSET_BITS=64" \
		USE_LOG=false USE_SELINUX=false \
		udevdir=$(UDEV_ROOT) -C $(UDEV_DIR) install
	install -m 0755 -D package/udev/init-udev $(TARGET_DIR)/etc/init.d/rc.udev
	install -m 0644 -D package/udev/udev-99-mmc.rules $(TARGET_DIR)/etc/udev/rules.d/99-mmc.rules
	install -m 0644 -D package/udev/udev-70-ide.rules $(TARGET_DIR)/etc/udev/rules.d/70-ide.rules
	install -m 0644 -D package/udev/udev-55-firmware.rules $(TARGET_DIR)/etc/udev/rules.d/55-firmware.rules
	echo 'udev_root=/dev' >> $(TARGET_DIR)/etc/udev/udev.conf
	install -m 0755 -D $(UDEV_DIR)/udevstart $(TARGET_DIR)/sbin/udevstart
	install -m 0755 -D $(UDEV_DIR)/udev $(TARGET_DIR)/sbin/udev

udev: uclibc $(TARGET_DIR)/$(UDEV_TARGET_BINARY)

udev-clean:
	$(MAKE) EXTRAS=extras/firmware DESTDIR=$(TARGET_DIR) CC=$(TARGET_CC) -C $(UDEV_DIR) uninstall
	-$(MAKE) -C $(UDEV_DIR) clean

udev-dirclean:
	rm -rf $(UDEV_DIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_UDEV)),y)
TARGETS+=udev
endif
