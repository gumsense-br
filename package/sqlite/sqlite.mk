#############################################################
#
# SQLite
#
#############################################################
SQLITE_SITE:=http://www.sqlite.org/
SQLITE_SOURCE:=sqlite-3.2.2.tar.gz
SQLITE_DIR:=$(BUILD_DIR)/sqlite-3.2.2
ifneq ($(BUILD_WITH_LARGEFILE),true)
	NOLARGE_FILE:=-DSQLITE_DISABLE_LFS
endif

$(DL_DIR)/$(SQLITE_SOURCE):
	$(WGET) -P $(DL_DIR) $(SQLITE_SITE)/$(SQLITE_SOURCE)

sqlite-source: $(DL_DIR)/$(SQLITE_SOURCE)

$(SQLITE_DIR)/.dist: $(DL_DIR)/$(SQLITE_SOURCE)
	zcat $(DL_DIR)/$(SQLITE_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	toolchain/patch-kernel.sh $(SQLITE_DIR) package/sqlite sqlite-*.patch
	touch  $(SQLITE_DIR)/.dist

$(SQLITE_DIR)/.configured: $(SQLITE_DIR)/.dist
	(cd $(SQLITE_DIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) \
		config_BUILD_CC="$(HOSTCC)" \
		config_TARGET_CFLAGS="$(TARGET_CFLAGS) $(NOLARGE_FILE)" \
		config_TARGET_CC="$(TARGET_CC)" \
		config_TARGET_READLINE_LIBS="-L$(TARGET_DIR)/usr/lib -L$(TARGET_DIR)/lib -lncurses -lreadline" \
		config_TARGET_READLINE_INC="-I$(STAGING_DIR)/usr/include" \
		./configure \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=/usr \
		--exec-prefix=/usr \
		--bindir=/usr/bin \
		--sbindir=/usr/sbin \
		--libexecdir=/usr/lib \
		--sysconfdir=/etc \
		--datadir=/usr/share \
		--localstatedir=/var \
		--mandir=/usr/man \
		--infodir=/usr/info \
		--with-gnu-ld \
		--enable-shared \
		--enable-static \
		--disable-tcl \
		--enable-tempstore \
		--enable-threadsafe \
		--enable-releasemode \
	);
	touch  $(SQLITE_DIR)/.configured

$(SQLITE_DIR)/.libs/libsqlite3-3.2.2.so: $(SQLITE_DIR)/.configured
	$(MAKE) $(JLEVEL) -C $(SQLITE_DIR)

$(STAGING_DIR)/lib/libsqlite3-3.2.2.so: $(SQLITE_DIR)/.libs/libsqlite3-3.2.2.so
	$(MAKE) $(JLEVEL) \
	    prefix=$(STAGING_DIR) \
	    exec_prefix=$(STAGING_DIR) \
	    bindir=$(STAGING_DIR)/bin \
	    sbindir=$(STAGING_DIR)/sbin \
	    libexecdir=$(STAGING_DIR)/lib \
	    datadir=$(STAGING_DIR)/share \
	    sysconfdir=$(STAGING_DIR)/etc \
	    localstatedir=$(STAGING_DIR)/var \
	    libdir=$(STAGING_DIR)/lib \
	    infodir=$(STAGING_DIR)/info \
	    mandir=$(STAGING_DIR)/man \
	    includedir=$(STAGING_DIR)/include \
	    -C $(SQLITE_DIR) install;
	chmod a-x $(STAGING_DIR)/lib/libsqlite3*so*
	touch $(STAGING_DIR)/lib/libsqlite3*so*
	rm -rf $(STAGING_DIR)/share/locale $(STAGING_DIR)/info \
		$(STAGING_DIR)/man $(STAGING_DIR)/share/doc

$(TARGET_DIR)/lib/libsqlite3-3.2.2.so: $(STAGING_DIR)/lib/libsqlite3-3.2.2.so
	rm -rf $(TARGET_DIR)/lib/libsqlite3*
	cp -a $(STAGING_DIR)/lib/libsqlite3*so*  $(TARGET_DIR)/lib/
	cp -a $(STAGING_DIR)/bin/sqlite3 $(TARGET_DIR)/usr/bin
	rm -f $(TARGET_DIR)/lib/libsqlite3.so $(TARGET_DIR)/lib/libsqlite3.la $(TARGET_DIR)/lib/libsqlite3.a
	(cd $(TARGET_DIR)/usr/lib; ln -fs /lib/libsqlite3-3.2.2.so libsqlite3.so)
	-$(STRIP) $(TARGET_DIR)/lib/libsqlite3*so*
	-$(STRIP) $(TARGET_DIR)/usr/bin/sqlite3

$(TARGET_DIR)/usr/lib/libsqlite3.a: $(STAGING_DIR)/lib/libsqlite3.a
	cp -dpf $(STAGING_DIR)/include/sqlite.h $(TARGET_DIR)/usr/include/
	cp -dpf $(STAGING_DIR)/lib/libsqlite*.a $(TARGET_DIR)/usr/lib/
	cp -dpf $(STAGING_DIR)/lib/libsqlite*.la $(TARGET_DIR)/usr/lib/
	touch -c $(TARGET_DIR)/usr/lib/libsqlite3.a

sqlite-headers: $(TARGET_DIR)/usr/lib/libsqlite3.a

sqlite-clean: 
	$(MAKE) -C $(SQLITE_DIR) clean

sqlite-dirclean: 
	rm -rf $(SQLITE_DIR) 

sqlite: uclibc ncurses readline $(TARGET_DIR)/lib/libsqlite3-3.2.2.so

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_SQLITE)),y)
TARGETS+=sqlite
endif
