#############################################################
#
# madplay
#
#############################################################

MADPLAY_VERSION=0.15.2b

# Don't alter below this line unless you (think) you know
# what you are doing! Danger, Danger!

MADPLAY_SOURCE=madplay-$(MADPLAY_VERSION).tar.gz
MADPLAY_SITE=ftp://ftp.mars.org/pub/mpeg
MADPLAY_DIR=$(BUILD_DIR)/${shell basename $(MADPLAY_SOURCE) .tar.gz}
MADPLAY_WORKDIR=$(BUILD_DIR)/madplay-$(MADPLAY_VERSION)

$(DL_DIR)/$(MADPLAY_SOURCE):
	$(WGET) -P $(DL_DIR) $(MADPLAY_SITE)/$(MADPLAY_SOURCE)

$(MADPLAY_DIR)/.unpacked:	$(DL_DIR)/$(MADPLAY_SOURCE)
	gzip -d -c $(DL_DIR)/$(MADPLAY_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	touch $(MADPLAY_DIR)/.unpacked

$(MADPLAY_DIR)/.configured:	$(MADPLAY_DIR)/.unpacked
	(cd $(MADPLAY_DIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) \
		CFLAGS="$(TARGET_CFLAGS)" \
		./configure \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=/usr \
		--sysconfdir=/etc \
		--disable-shared \
		$(DISABLE_NLS) \
		);
	touch  $(MADPLAY_DIR)/.configured

$(MADPLAY_WORKDIR)/madplay:	$(MADPLAY_DIR)/.configured
	$(MAKE) CPPFLAGS="-I$(LIBMAD_DIR) -I$(LIBID3TAG_DIR)" LDFLAGS="-L$(LIBMAD_DIR) -L$(LIBID3TAG_DIR)" -C $(MADPLAY_DIR)

$(TARGET_DIR)/usr/bin/madplay: 	$(MADPLAY_WORKDIR)/madplay
	mkdir -p $(TARGET_DIR)/usr/bin
	cp -f $(MADPLAY_WORKDIR)/madplay $(TARGET_DIR)/usr/bin
	$(STRIP) $(TARGET_DIR)/usr/bin/madplay

madplay:	uclibc zlib libid3tag libmad $(TARGET_DIR)/usr/bin/madplay

madplay-source: $(DL_DIR)/$(MADPLAY_SOURCE)

madplay-clean:
	@if [ -d $(MADPLAY_WORKDIR)/Makefile ] ; then \
		$(MAKE) -C $(MADPLAY_WORKDIR) clean ; \
	fi;

madplay-dirclean:
	rm -rf $(MADPLAY_DIR) $(MADPLAY_WORKDIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_MADPLAY)),y)
TARGETS+=madplay
endif
