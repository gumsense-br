#############################################################
#
# libpcre
#
#############################################################
#
LIBPCRE_SOURCE_URL=ftp://ftp.sourceforge.net/pub/sourceforge/p/pc/pcre
LIBPCRE_SOURCE=pcre-4.3.tar.bz2
LIBPCRE_BUILD_DIR=$(BUILD_DIR)/pcre-4.3

$(DL_DIR)/$(LIBPCRE_SOURCE):
	 $(WGET) -P $(DL_DIR) $(TCPDUMP_SOURCE_URL)/$(LIBPCRE_SOURCE)

$(LIBPCRE_BUILD_DIR)/.unpacked: $(DL_DIR)/$(LIBPCRE_SOURCE)
	bzcat $(DL_DIR)/$(LIBPCRE_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	toolchain/patch-kernel.sh $(LIBPCRE_BUILD_DIR) package/libpcre pcre*.patch
	touch $(LIBPCRE_BUILD_DIR)/.unpacked

$(LIBPCRE_BUILD_DIR)/.configured: $(LIBPCRE_BUILD_DIR)/.unpacked
	(cd $(LIBPCRE_BUILD_DIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) CFLAGS="$(TARGET_CFLAGS)" \
		CC_FOR_BUILD="$(HOSTCC)" CFLAGS_FOR_BUILD="-O2" \
		./configure \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix="$(STAGING_DIR)" \
		--with-pic \
		--disable-shared \
	);
	touch  $(LIBPCRE_BUILD_DIR)/.configured

$(LIBPCRE_BUILD_DIR)/libpcre.la: $(LIBPCRE_BUILD_DIR)/.configured
	$(MAKE) $(JLEVEL) -C $(LIBPCRE_BUILD_DIR) libpcre.la

$(STAGING_DIR)/lib/libpcre.a: $(LIBPCRE_BUILD_DIR)/libpcre.la
	$(MAKE) $(JLEVEL) -C $(LIBPCRE_BUILD_DIR) install

.PHONY: libpcre-source libpcre

libpcre: $(STAGING_DIR)/lib/libpcre.a

libpcre-source: $(DL_DIR)/$(LIBPCRE_SOURCE)

libpcre-clean:
	-$(MAKE) -C $(LIBPCRE_BUILD_DIR) clean

libpcre-dirclean:
	rm -rf $(LIBPCRE_BUILD_DIR) $(TCPDUMP_BUILD_DIR)
#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_LIBPCRE)),y)
TARGETS+=libpcre
endif
