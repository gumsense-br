#############################################################
#
# liblo: lightweight OSC implementation by Steve Harris
# (http://plugin.org.uk/liblo)
# 
# Simon de Bakker <simon@v2.nl>
#
#############################################################

# TARGETS
LIBLO_SITE:=http://plugin.org.uk/liblo/releases
LIBLO_DIR:=$(BUILD_DIR)/liblo-0.22
LIBLO_SOURCE:=liblo-0.22.tar.gz

$(DL_DIR)/$(LIBLO_SOURCE):
	$(WGET) -P $(DL_DIR) $(LIBLO_SITE)/$(LIBLO_SOURCE)

$(LIBLO_DIR)/.dist: $(DL_DIR)/$(LIBLO_SOURCE)
	gunzip -c $(DL_DIR)/$(LIBLO_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	toolchain/patch-kernel.sh $(LIBLO_DIR) package/liblo liblo-\*.patch
	touch  $(LIBLO_DIR)/.dist

$(LIBLO_DIR)/.configured: $(LIBLO_DIR)/.dist
	(cd $(LIBLO_DIR); rm -rf config.cache; \
		BUILD_CC=$(TARGET_CC) HOSTCC=$(HOSTCC) \
		$(TARGET_CONFIGURE_OPTS) \
		CFLAGS="$(TARGET_CFLAGS)" \
		./configure \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=/usr \
		--exec-prefix=/usr \
		--bindir=/usr/bin \
		--sbindir=/usr/sbin \
		--sysconfdir=/etc \
		--datadir=/usr/share \
		--localstatedir=/var \
		--mandir=/usr/man \
		--infodir=/usr/info \
		--with-terminfo-dirs=/usr/share/terminfo \
		--with-default-terminfo-dir=/usr/share/terminfo \
		--libdir=$(STAGING_DIR)/lib \
	);
	touch  $(LIBLO_DIR)/.configured

$(LIBLO_DIR)/src/.libs: $(LIBLO_DIR)/.configured
	$(MAKE) $(JLEVEL) CC=$(TARGET_CC) -C $(LIBLO_DIR)/src
	$(MAKE) $(JLEVEL) CC=$(TARGET_CC) -C $(LIBLO_DIR)/lo

$(STAGING_DIR)/lib/liblo.so.0.5.0: $(LIBLO_DIR)/src/.libs
	$(MAKE) prefix=$(STAGING_DIR) -C $(LIBLO_DIR)/src install
	$(MAKE) prefix=$(STAGING_DIR) -C $(LIBLO_DIR)/lo install

$(TARGET_DIR)/lib/liblo.so.0.5.0: $(STAGING_DIR)/lib/liblo.so.0.5.0
	cp -dpf $(STAGING_DIR)/lib/liblo.so* $(TARGET_DIR)/lib/
	-$(STRIP) $(TARGET_DIR)/lib/liblo.so*

liblo-source: $(DL_DIR)/$(LIBLO_SOURCE)

liblo-clean: 
	rm -f $(STAGING_DIR)/lib/liblo.so* $(TARGET_DIR)/lib/liblo.so*
	-$(MAKE) -C $(LIBLO_DIR) clean

liblo-dirclean: 
	rm -rf $(LIBLO_DIR)

liblo: $(TARGET_DIR)/lib/liblo.so.0.5.0

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_LIBLO)),y)
TARGETS+=liblo
endif
