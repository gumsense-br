#############################################################
#
# gpsd
#
#############################################################
GPSD_SOURCE:=gpsd-2.33.tar.gz
GPSD_SITE:=http://download.berlios.de/gpsd/
GPSD_DIR:=$(BUILD_DIR)/gpsd-2.33
GPSD_CAT:=zcat
GPSD_BINARY:=gpsd
GPSD_TARGET_BINARY:=usr/sbin/gpsd
LIBGPS:=libgps.a

$(DL_DIR)/$(GPSD_SOURCE):
	 $(WGET) -P $(DL_DIR) $(GPSD_SITE)/$(GPSD_SOURCE)

gpsd-source: $(DL_DIR)/$(GPSD_SOURCE)

$(GPSD_DIR)/configure: $(DL_DIR)/$(GPSD_SOURCE)
	$(GPSD_CAT) $(DL_DIR)/$(GPSD_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	toolchain/patch-kernel.sh $(GPSD_DIR) package/gpsd gpsd-*.patch

$(GPSD_DIR)/Makefile: $(GPSD_DIR)/configure
	(cd $(GPSD_DIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) \
		CC="$(TARGET_CC)" \
		CFLAGS="$(TARGET_CFLAGS)" \
		LDFLAGS="-lm" \
		./configure \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=/usr \
		--exec-prefix=/usr \
		--bindir=/usr/bin \
		--sbindir=/usr/sbin \
		--libexecdir=/usr/lib \
		--sysconfdir=/etc \
		--datadir=/usr/share \
		--localstatedir=/var \
		--mandir=/usr/man \
		--infodir=/usr/info \
		--disable-shared \
		--enable-static \
		--without-x \
		$(DISABLE_NLS) \
		--disable-tsip \
		--disable-fv18 \
		--disable-tripmate \
		--disable-earthmate \
		--disable-itrax \
		--disable-italk \
		--disable-garmin \
		--disable-evermore \
		--disable-ntpshm \
	);

$(GPSD_DIR)/$(GPSD_BINARY): $(GPSD_DIR)/Makefile
	$(MAKE) $(JLEVEL) -C $(GPSD_DIR) packet_names.h gpsd gpspipe

$(TARGET_DIR)/$(GPSD_TARGET_BINARY): $(GPSD_DIR)/$(GPSD_BINARY)
	$(MAKE) DESTDIR=$(TARGET_DIR) -C $(GPSD_DIR) install-sbinPROGRAMS
	$(MAKE) DESTDIR=$(TARGET_DIR) -C $(GPSD_DIR) install-binPROGRAMS
	rm -rf $(TARGET_DIR)/share/locale $(TARGET_DIR)/usr/info \
		$(TARGET_DIR)/usr/man $(TARGET_DIR)/usr/share/doc
	$(STRIP) $(TARGET_DIR)/$(GPSD_TARGET_BINARY)

$(STAGING_DIR)/lib/$(LIBGPS): $(GPSD_DIR)/$(GPSD_BINARY)
	$(MAKE) DESTDIR=$(STAGING_DIR) -C $(GPSD_DIR) install-includeHEADERS install-libLTLIBRARIES

libgps: uclibc $(STAGING_DIR)/lib/$(LIBGPS)

gpsd: uclibc $(TARGET_DIR)/$(GPSD_TARGET_BINARY)

gpsd-clean:
	$(MAKE) DESTDIR=$(TARGET_DIR) CC=$(TARGET_CC) -C $(GPSD_DIR) uninstall
	-$(MAKE) -C $(GPSD_DIR) clean

gpsd-dirclean:
	rm -rf $(GPSD_DIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_GPSD)),y)
TARGETS+=gpsd
endif
