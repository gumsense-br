#!/bin/sh

# CRH -- this should really be invoked from /etc/network/interfaces
# right now it's not really in a suitable form for that, but checking
# it in anyway as a basis to work from

PATH=$PATH
ZCIP=/sbin/zcip
IFNAME=eth0
SCRIPT=/usr/share/zcip/zcip.script

# SHOULD use "-r 169.254.x.x" to reclaim the last value
# it should have been saved away somewhere by $SCRIPT
FLAGS=""

case "$1" in
start|restart)
	ip link set $IFNAME up
	exec $ZCIP $FLAGS $IFNAME $SCRIPT
	;;
stop)
	# FIXME should probably save and use daemon's PID
	;;
status)
	;;
*)
	echo "Usage: $0 {start|stop|status|restart}"
	exit 1
	;;
esac
