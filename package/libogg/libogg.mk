#############################################################
#
# libogg, shamelessly modified from libid3tag
#
#############################################################

LIBOGG_VERSION=1.1.3

# Don't alter below this line unless you (think) you know
# what you are doing! Danger, Danger!

LIBOGG_SOURCE=libogg-$(LIBOGG_VERSION).tar.gz
LIBOGG_SITE=http://downloads.xiph.org/releases/ogg
LIBOGG_DIR=$(BUILD_DIR)/${shell basename $(LIBOGG_SOURCE) .tar.gz}
LIBOGG_WORKDIR=$(BUILD_DIR)/libogg-$(LIBOGG_VERSION)

$(DL_DIR)/$(LIBOGG_SOURCE):
	$(WGET) -P $(DL_DIR) $(LIBOGG_SITE)/$(LIBOGG_SOURCE)

$(LIBOGG_DIR)/.unpacked:	$(DL_DIR)/$(LIBOGG_SOURCE)
	gzip -d -c $(DL_DIR)/$(LIBOGG_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	touch $(LIBOGG_DIR)/.unpacked

$(LIBOGG_DIR)/.configured: $(LIBOGG_DIR)/.unpacked
	(cd $(LIBOGG_DIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) \
		CFLAGS="$(TARGET_CFLAGS)" \
		./configure \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=$(STAGING_DIR)/usr \
		--exec-prefix=$(STAGING_DIR)/usr \
		--bindir=$(STAGING_DIR)/usr/bin \
		--sbindir=$(STAGING_DIR)/usr/sbin \
		--libexecdir=$(STAGING_DIR)/usr/lib \
		--sysconfdir=$(STAGING_DIR)/etc \
		--datadir=$(STAGING_DIR)/usr/share \
		--localstatedir=$(STAGING_DIR)/var \
		--mandir=$(STAGING_DIR)/usr/man \
		--infodir=$(STAGING_DIR)/usr/info \
		--disable-shared \
		$(DISABLE_NLS) \
	);
	touch  $(LIBOGG_DIR)/.configured

$(LIBOGG_WORKDIR)/.libs:	$(LIBOGG_DIR)/.configured
	$(MAKE) $(JLEVEL) -C $(LIBOGG_WORKDIR)

$(STAGING_DIR)/lib/libogg.a: 	$(LIBOGG_WORKDIR)/.libs
	$(MAKE) PREFIX=$(STAGING_DIR) -C $(LIBOGG_WORKDIR) install

libogg:	uclibc $(STAGING_DIR)/lib/libogg.a

libogg-source: $(DL_DIR)/$(LIBOGG_SOURCE)

libogg-clean:
	@if [ -d $(LIBOGG_WORKDIR)/Makefile ] ; then \
		$(MAKE) -C $(LIBOGG_WORKDIR) clean ; \
	fi;

libogg-dirclean:
	rm -rf $(LIBOGG_DIR) $(LIBOGG_WORKDIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_LIBOGG)),y)
TARGETS+=libogg
endif
