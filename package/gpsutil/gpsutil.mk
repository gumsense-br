#############################################################
#
# gpsutil
#
#############################################################
GPSUTIL_SOURCE:=gpsutil.tar.bz2
GPSUTIL_SITE:=http://files.gumstix.com
GPSUTIL_DIR:=$(BUILD_DIR)/gpsutil_2
GPSUTIL_CAT:=bzcat
GPSUTIL_BINARY:=gpsutil
GPSUTIL_TARGET_BINARY:=usr/bin/gpsutil

$(DL_DIR)/$(GPSUTIL_SOURCE):
	 $(WGET) -P $(DL_DIR) $(GPSUTIL_SITE)/$(GPSUTIL_SOURCE)

gpsutil-source: $(DL_DIR)/$(GPSUTIL_SOURCE)

$(GPSUTIL_DIR)/Makefile: $(DL_DIR)/$(GPSUTIL_SOURCE)
	$(GPSUTIL_CAT) $(DL_DIR)/$(GPSUTIL_SOURCE) | tar -C "$(BUILD_DIR)" $(TAR_OPTIONS) -

$(GPSUTIL_DIR)/$(GPSUTIL_BINARY): $(GPSUTIL_DIR)/Makefile
	$(MAKE) $(JLEVEL) CC="$(TARGET_CC)" CFLAGS="$(TARGET_CFLAGS) -DLINUX" -C "$(GPSUTIL_DIR)"

$(TARGET_DIR)/$(GPSUTIL_TARGET_BINARY): $(GPSUTIL_DIR)/$(GPSUTIL_BINARY)
	$(STRIP) "$<"
	install -m 0755 $< $@

gpsutil: uclibc $(TARGET_DIR)/$(GPSUTIL_TARGET_BINARY)

gpsutil-clean:
	rm -f $(TARGET_DIR)/$(GPSUTIL_TARGET_BINARY)
	-$(MAKE) $(JLEVEL) -C "$(GPSUTIL_DIR)" clean

gpsutil-dirclean:
	rm -rf $(GPSUTIL_DIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_GPSUTIL)),y)
TARGETS+=gpsutil
endif
