#############################################################
#
# speex
#
#############################################################

SPEEX_NAME=speex
SPEEX_VERSION=1.2beta1

SPEEXextraCFLAGS=--no-builtin-exp --no-builtin-cos --no-builtin-sin --no-builtin-log 

# Don't alter below this line unless you (think) you know
# what you are doing! Danger, Danger!

SPEEX_SITE=http://downloads.us.xiph.org/releases/speex
SPEEX_SOURCE=$(SPEEX_NAME)-$(SPEEX_VERSION).tar.gz
SPEEX_DIR=$(BUILD_DIR)/${shell basename $(SPEEX_SOURCE) .tar.gz}

$(DL_DIR)/$(SPEEX_SOURCE):
	$(WGET) -P $(DL_DIR) $(SPEEX_SITE)/$(SPEEX_SOURCE)

$(SPEEX_DIR)/.unpacked:	$(DL_DIR)/$(SPEEX_SOURCE)
	gzip -d -c $(DL_DIR)/$(SPEEX_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	touch $(SPEEX_DIR)/.unpacked

$(SPEEX_DIR)/.configured:	$(SPEEX_DIR)/.unpacked
	(cd $(SPEEX_DIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) \
		CFLAGS="$(TARGET_CFLAGS) $(SPEEXextraCFLAGS)" \
		./configure \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=$(STAGING_DIR)/usr \
		--exec-prefix=$(STAGING_DIR)/usr \
		--bindir=$(STAGING_DIR)/usr/bin \
		--sbindir=$(STAGING_DIR)/usr/sbin \
		--libexecdir=$(STAGING_DIR)/usr/lib \
		--sysconfdir=$(STAGING_DIR)/etc \
		--datadir=$(STAGING_DIR)/usr/share \
		--localstatedir=$(STAGING_DIR)/var \
		--mandir=$(STAGING_DIR)/usr/man \
		--infodir=$(STAGING_DIR)/usr/info \
		--with-ogg-dir=$(STAGING_DIR)/usr \
		--disable-shared \
		--disable-oggtest \
		--enable-fixed-point \
		--enable-arm-asm \
		$(DISABLE_NLS) \
		);
	touch  $(SPEEX_DIR)/.configured

$(SPEEX_DIR)/.maked:	$(SPEEX_DIR)/.configured
	$(MAKE1) -C $(SPEEX_DIR)
	touch $(SPEEX_DIR)/.maked


$(SPEEX_DIR)/.installed:	$(SPEEX_DIR)/.maked
	$(MAKE1) PREFIX=$(STAGING_DIR) -C $(SPEEX_DIR) install
	touch $(SPEEX_DIR)/.installed

$(SPEEX_NAME): uclibc libogg zlib $(SPEEX_DIR)/.installed

$(SPEEX_NAME)-source: $(DL_DIR)/$(SPEEX_SOURCE)

$(SPEEX_NAME)-clean:
	@if [ -d $(SPEEX_DIR)/Makefile ] ; then \
		$(MAKE1) -C $(SPEEX_DIR) clean ; \
	fi;

$(SPEEX_NAME)-dirclean:
	rm -rf $(SPEEX_DIR) 

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_SPEEX)),y)
TARGETS+=speex
endif
