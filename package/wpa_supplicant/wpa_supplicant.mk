#############################################################
#
# Driver for wpa_supplicant app
#
#############################################################
WPA_SUPPLICANT_VERSION:=0.4.9
WPA_SUPPLICANT_SOURCE:=wpa_supplicant-$(WPA_SUPPLICANT_VERSION).tar.gz
WPA_SUPPLICANT_SITE:=http://hostap.epitest.fi/releases
WPA_SUPPLICANT_DIR:=$(BUILD_DIR)/wpa_supplicant-$(WPA_SUPPLICANT_VERSION)
WPA_SUPPLICANT_CAT:=zcat
WPA_SUPPLICANT_BINARY=$(WPA_SUPPLICANT_DIR)/wpa_supplicant
WPA_SUPPLICANT_TARGET=$(TARGET_DIR)/sbin/wpa_supplicant

$(DL_DIR)/$(WPA_SUPPLICANT_SOURCE):
	 $(WGET) -P $(DL_DIR) $(WPA_SUPPLICANT_SITE)/$(WPA_SUPPLICANT_SOURCE)

wpa_supplicant-source: $(DL_DIR)/$(WPA_SUPPLICANT_SOURCE)

$(WPA_SUPPLICANT_DIR)/.unpacked: $(DL_DIR)/$(WPA_SUPPLICANT_SOURCE)
	$(WPA_SUPPLICANT_CAT) $(DL_DIR)/$(WPA_SUPPLICANT_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	touch $(WPA_SUPPLICANT_DIR)/.unpacked

$(WPA_SUPPLICANT_DIR)/.patched: $(WPA_SUPPLICANT_DIR)/.unpacked
	(cd $(WPA_SUPPLICANT_DIR); QUILT_PATCHES=$(BASE_DIR)/package/wpa_supplicant $(QUILT) push -a)
	echo $(BASE_DIR)/package/wpa_supplicant > $@

$(WPA_SUPPLICANT_BINARY): $(WPA_SUPPLICANT_DIR)/.patched
	(cd $(WPA_SUPPLICANT_DIR); \
	rm -f .config; \
	make mkconfig ARM; \
	$(MAKE) CROSS="$(KERNEL_CROSS)" EXTRA_CFLAGS="$(TARGET_CFLAGS)" OPENSSLDIR="$(STAGING_DIR)/lib";)
	touch $@

$(WPA_SUPPLICANT_TARGET): $(WPA_SUPPLICANT_BINARY)
	mkdir -p $(TARGET_DIR)/bin $(TARGET_DIR)/sbin
	install -m 0755 $(WPA_SUPPLICANT_DIR)/wpa_cli $(TARGET_DIR)/bin/wpa_cli
	install -m 0755 $(WPA_SUPPLICANT_DIR)/wpa_passphrase $(TARGET_DIR)/bin/wpa_passphrase
	install -m 0755 $< $@
	$(SAFE_STRIP) $(TARGET_DIR)/bin/wpa_* $@

wpa_supplicant: uclibc openssl $(WPA_SUPPLICANT_TARGET)

wpa_supplicant-clean:
	rm -f $(WPA_SUPPLICANT_TARGET)
	-$(MAKE) -C $(WPA_SUPPLICANT_DIR) clean

wpa_supplicant-dirclean:
	rm -rf $(WPA_SUPPLICANT_DIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_WPA_SUPPLICANT)),y)
TARGETS+=wpa_supplicant
endif
