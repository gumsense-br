#############################################################
#
# Tcl
#
#############################################################


#TCL_SOURCE=http://$(BR2_SOURCEFORGE_MIRROR).dl.sourceforge.net/sourceforge/tcl/tcl8.4.11-src.tar.gz
TCL_EXTRACTED=tcl8.4.14
TCL_DIR=$(BUILD_DIR)/${TCL_EXTRACTED}/unix
TCL_TARBALL=tcl8.4.14-src.tar.gz
TCL_SOURCE=http://$(BR2_SOURCEFORGE_MIRROR).dl.sourceforge.net/sourceforge/tcl/${TCL_TARBALL}
TCL_PATCH=package/tcl/tcl.patch
TCL_DEPS=
TCL_BIN=$(TARGET_DIR)/usr/bin/tclsh8.4

$(DL_DIR)/${TCL_TARBALL}:
	$(WGET) -O $@ $(TCL_SOURCE)

$(TCL_DIR)/.unpacked: $(DL_DIR)/${TCL_TARBALL}
	tar -C $(BUILD_DIR) -xzvf $<
	cat $(TCL_PATCH) | patch -p1 -d $(BUILD_DIR)/${TCL_EXTRACTED}
	touch $@

$(TCL_DIR)/.configured: $(TCL_DIR)/.unpacked
	(cd $(TCL_DIR); rm -rf config.cache; \
	        autoconf; \
		$(TARGET_CONFIGURE_OPTS) \
		CFLAGS="$(TARGET_CFLAGS)" \
		./configure \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=/usr \
		--includedir=/include \
	);
	touch $@

$(TCL_DIR)/.built: $(TCL_DIR)/.configured
	$(MAKE) -C $(TCL_DIR)
	touch $@

$(TCL_DIR)/.tested: $(TCL_DIR)/.built
	$(MAKE) -C $(TCL_DIR)
	touch $@

$(TCL_BIN): $(TCL_DIR)/.tested
	$(MAKE) DESTDIR=$(TARGET_DIR) -C $(TCL_DIR) install install-private-headers

tcl:	uclibc $(DEPS) $(TCL_BIN)

tcl-source: $(DL_DIR)/${TCL_TARBALL}

tcl-clean:
	@if [ -d $(TCL_DIR)/Makefile ] ; then \
		$(MAKE) -C $(TCL_DIR) clean ; \
	fi;

tcl-dirclean:
	rm -rf $(TCL_DIR)
#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_TCL)),y)
TARGETS+=tcl
endif
