#############################################################
#
# schedule utilities
#
#############################################################
SCHEDUTILS_VERSION:=1.5.0
SCHEDUTILS_SOURCE:=schedutils-$(SCHEDUTILS_VERSION).tar.gz
SCHEDUTILS_SITE:=http://rlove.org/schedutils/
SCHEDUTILS_DIR:=$(BUILD_DIR)/schedutils-$(SCHEDUTILS_VERSION)
SCHEDUTILS_CAT:=zcat
SCHEDUTILS_BINARY:=chrt
SCHEDUTILS_TARGET_BINARY:=usr/sbin/chrt

$(DL_DIR)/$(SCHEDUTILS_SOURCE):
	$(WGET) -P $(DL_DIR) $(SCHEDUTILS_SITE)/$(SCHEDUTILS_SOURCE)

schedutils-source: $(DL_DIR)/$(SCHEDUTILS_SOURCE)

$(SCHEDUTILS_DIR)/.unpacked: $(DL_DIR)/$(SCHEDUTILS_SOURCE)
	$(SCHEDUTILS_CAT) $(DL_DIR)/$(SCHEDUTILS_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	toolchain/patch-kernel.sh $(SCHEDUTILS_DIR) package/schedutils/ schedutils\*.patch
	touch $(SCHEDUTILS_DIR)/.unpacked

$(SCHEDUTILS_DIR)/.configured: $(SCHEDUTILS_DIR)/.unpacked
	touch $(SCHEDUTILS_DIR)/.configured

$(SCHEDUTILS_DIR)/$(SCHEDUTILS_BINARY): $(SCHEDUTILS_DIR)/.configured
	CC="$(TARGET_CC)" CFLAGS="$(TARGET_CFLAGS)" $(MAKE) -C $(SCHEDUTILS_DIR)

$(TARGET_DIR)/$(SCHEDUTILS_TARGET_BINARY): $(SCHEDUTILS_DIR)/$(SCHEDUTILS_BINARY)
	install -m 755 $(SCHEDUTILS_DIR)/ionice $(TARGET_DIR)/usr/sbin/ionice
	install -m 755 $(SCHEDUTILS_DIR)/$(SCHEDUTILS_BINARY) $(TARGET_DIR)/$(SCHEDUTILS_TARGET_BINARY)

schedutils: uclibc $(TARGET_DIR)/$(SCHEDUTILS_TARGET_BINARY)

schedutils-clean:
	rm -f $(TARGET_DIR)/usr/sbin/chrt
	rm -f $(TARGET_DIR)/$(SCHEDUTILS_TARGET_BINARY)
	-$(MAKE) -C $(SCHEDUTILS_DIR) clean

schedutils-dirclean:
	rm -rf $(SCHEDUTILS_DIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_SCHEDUTILS)),y)
TARGETS+=schedutils
endif
