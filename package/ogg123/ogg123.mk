#############################################################
#
# ogg123, link with tremor library
#
#############################################################
OGG123_SOURCE:=vorbis-tools-1.0.1.tar.gz
OGG123_SITE:=http://www.vorbis.com/files/1.0.1/unix
OGG123_DIR:=$(BUILD_DIR)/vorbis-tools-1.0.1
OGG123_BINARY:=ogg123/ogg123
OGG123_CAT:=zcat

OGG123_TARGET_BINARY:=usr/bin/ogg123


$(DL_DIR)/$(OGG123_SOURCE):
	 $(WGET) -P $(DL_DIR) $(OGG123_SITE)/$(OGG123_SOURCE)

ogg123-source: $(DL_DIR)/$(OGG123_SOURCE)

$(OGG123_DIR)/.unpacked: $(DL_DIR)/$(OGG123_SOURCE)
	$(OGG123_CAT) $(DL_DIR)/$(OGG123_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	toolchain/patch-kernel.sh $(OGG123_DIR) package/ogg123 vorbis-*.patch
	(cd $(OGG123_DIR); autoconf )
	touch $(OGG123_DIR)/.unpacked

$(OGG123_DIR)/.configured: $(OGG123_DIR)/.unpacked
	(cd $(OGG123_DIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) \
		CFLAGS="$(TARGET_CFLAGS)" \
		./configure \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=/usr \
		--disable-shared \
		--disable-oggdec \
		--disable-oggenc \
		--disable-ogginfo \
		--disable-vcut \
		--disable-vorbiscomment \
		--disable-oggtest \
		--disable-vorbistest \
		--disable-aotest \
		--disable-curltest \
		--without-flac \
		--without-speex \
		$(DISABLE_NLS) \
	);
	touch  $(OGG123_DIR)/.configured

$(OGG123_DIR)/$(OGG123_BINARY): $(OGG123_DIR)/.configured
	$(MAKE) CC=$(TARGET_CC) -C $(OGG123_DIR)

$(TARGET_DIR)/$(OGG123_TARGET_BINARY): $(OGG123_DIR)/$(OGG123_BINARY)
	install -m 755 $(OGG123_DIR)/$(OGG123_BINARY) $(TARGET_DIR)/$(OGG123_TARGET_BINARY)
	$(STRIP) $(TARGET_DIR)/$(OGG123_TARGET_BINARY)

ogg123: uclibc libao libogg tremor $(TARGET_DIR)/$(OGG123_TARGET_BINARY)

ogg123-clean:
	$(MAKE) DESTDIR=$(TARGET_DIR) CC=$(TARGET_CC) -C $(OGG123_DIR) uninstall
	-$(MAKE) -C $(OGG123_DIR) clean

ogg123-dirclean:
	rm -rf $(OGG123_DIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_OGG123)),y)
TARGETS+=ogg123
endif
