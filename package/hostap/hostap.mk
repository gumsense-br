#############################################################
#
# hostap
#
#############################################################
HOSTAP_DRIVER_VERSION=0.4.7
HOSTAPD_VERSION=0.4.7
HOSTAP_UTILS_VERSION=0.4.7
HOSTAP_DIR=$(BUILD_DIR)/hostap-combined-$(HOSTAP_DRIVER_VERSION)

HOSTAP_URL=http://hostap.epitest.fi/releases
HOSTAP_DRIVER_SOURCE=hostap-driver-${HOSTAP_DRIVER_VERSION}
HOSTAP_UTILS_SOURCE=hostap-utils-${HOSTAP_UTILS_VERSION}
HOSTAPD_SOURCE=hostapd-${HOSTAPD_VERSION}

pathsearch = $(firstword $(wildcard $(addsuffix /$(1),$(subst :, ,/sbin))))
DEPMOD_TMP := $(call pathsearch,depmod)
ifneq "$(DEPMOD_TMP)" "/sbin/depmod"
DEPMOD=$(BUSYBOX_DIR)/examples/depmod.pl
echo DEPMOD1=$(DEPMOD)
else
DEPMOD=/sbin/depmod
echo DEPMOD2=$(DEPMOD)
endif

$(DL_DIR)/$(HOSTAPD_SOURCE).tar.gz:
	echo "Running wget"
	$(WGET) -nH -P $(DL_DIR) $(HOSTAP_URL)/$(HOSTAPD_SOURCE).tar.gz -O $(DL_DIR)/$(HOSTAPD_SOURCE).tar.gz

$(DL_DIR)/$(HOSTAP_DRIVER_SOURCE).tar.gz:
	echo "Running wget"
	$(WGET) -nH -P $(DL_DIR) $(HOSTAP_URL)/$(HOSTAP_DRIVER_SOURCE).tar.gz -O $(DL_DIR)/$(HOSTAP_DRIVER_SOURCE).tar.gz

$(DL_DIR)/$(HOSTAP_UTILS_SOURCE).tar.gz:
	echo "Running wget"
	$(WGET) -nH -P $(DL_DIR) $(HOSTAP_URL)/$(HOSTAP_UTILS_SOURCE).tar.gz -O $(DL_DIR)/$(HOSTAP_UTILS_SOURCE).tar.gz 

$(HOSTAP_DIR)/.unpacked: $(DL_DIR)/$(HOSTAPD_SOURCE).tar.gz $(DL_DIR)/$(HOSTAP_DRIVER_SOURCE).tar.gz $(DL_DIR)/$(HOSTAP_UTILS_SOURCE).tar.gz
	zcat $(DL_DIR)/$(HOSTAP_DRIVER_SOURCE).tar.gz | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	zcat $(DL_DIR)/$(HOSTAP_UTILS_SOURCE).tar.gz | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	zcat $(DL_DIR)/$(HOSTAPD_SOURCE).tar.gz | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	mkdir -p $(HOSTAP_DIR)
	cp -rf $(BUILD_DIR)/$(HOSTAP_DRIVER_SOURCE)/* $(HOSTAP_DIR)
	mkdir -p $(HOSTAP_DIR)/utils
	cp -rf $(BUILD_DIR)/$(HOSTAP_UTILS_SOURCE)/* $(HOSTAP_DIR)/utils
	mkdir -p $(HOSTAP_DIR)/hostapd
	cp -rf $(BUILD_DIR)/$(HOSTAPD_SOURCE)/* $(HOSTAP_DIR)/hostapd
	toolchain/patch-kernel.sh $(HOSTAP_DIR) package/hostap hostap-main*.patch
	toolchain/patch-kernel.sh $(HOSTAP_DIR)/driver/etc package/hostap hostap-conf*.patch
	touch $(HOSTAP_DIR)/.unpacked

$(HOSTAP_DIR)/.configured: $(HOSTAP_DIR)/.unpacked
	$(SED) "s,/.*#define PRISM2_DOWNLOAD_SUPPORT.*/,#define PRISM2_DOWNLOAD_SUPPORT,g" \
		$(HOSTAP_DIR)/driver/modules/hostap_config.h
	cp -f package/hostap/hostap.config $(HOSTAP_DIR)/hostapd/.config
	touch  $(HOSTAP_DIR)/.configured

$(HOSTAP_DIR)/utils/hostap_crypt_conf: $(HOSTAP_DIR)/.configured
	#$(MAKE) $(JLEVEL) -C $(HOSTAP_DIR) DESTDIR="$(TARGET_DIR)" KERNEL_PATH="$(LINUX_DIR)" CROSS_COMPILE="$(KERNEL_CROSS)" ARCH=$(ARCH) CC="$(TARGET_CC)" EXTRA_CFLAGS="$(TARGET_CFLAGS) -Wall -MMD" 2.6
	$(MAKE) $(JLEVEL) -C $(HOSTAP_DIR)/utils DESTDIR="$(TARGET_DIR)" KERNEL_PATH="$(LINUX_DIR)" CROSS_COMPILE="$(KERNEL_CROSS)" ARCH=$(ARCH) CC=$(TARGET_CC) EXTRA_CFLAGS="-Wall $(TARGET_CFLAGS) -I../driver/modules"
	$(MAKE) $(JLEVEL) -C $(HOSTAP_DIR)/hostapd DESTDIR="$(TARGET_DIR)" KERNEL_PATH="$(LINUX_DIR)" CROSS_COMPILE="$(KERNEL_CROSS)" ARCH=$(ARCH) CC=$(TARGET_CC) EXTRA_CFLAGS="-Wall $(TARGET_CFLAGS) -I../driver/modules -I../utils -I../wpa_supplicant"
	#touch -c $(HOSTAP_DIR)/driver/modules/hostap.o

$(TARGET_DIR)/usr/bin/hostap_crypt_conf: $(HOSTAP_DIR)/utils/hostap_crypt_conf
	# Copy the modules in
	#$(MAKE) -C $(HOSTAP_DIR) DESTDIR="$(TARGET_DIR)" KERNEL_PATH="$(LINUX_DIR)" CROSS_COMPILE="$(KERNEL_CROSS)" ARCH=$(ARCH) CC="$(TARGET_CC)" EXTRA_CFLAGS="$(TARGET_CFLAGS) -Wall -MMD" install_2.6
	# Copy the pcmcia-cs conf file
	-mkdir -p $(TARGET_DIR)/etc/pcmcia
	cp -Rf $(HOSTAP_DIR)/driver/etc/hostap_cs.conf $(TARGET_DIR)/etc/pcmcia/
	# Copy The Utils
	cp -Rf $(HOSTAP_DIR)/utils/hostap_crypt_conf $(TARGET_DIR)/usr/bin/
	cp -Rf $(HOSTAP_DIR)/utils/hostap_diag $(TARGET_DIR)/usr/bin/
	cp -Rf $(HOSTAP_DIR)/utils/prism2_param $(TARGET_DIR)/usr/bin/
	cp -Rf $(HOSTAP_DIR)/utils/prism2_srec $(TARGET_DIR)/usr/bin/
	# Copy hostapd
	cp -Rf $(HOSTAP_DIR)/hostapd/hostapd $(TARGET_DIR)/usr/sbin/

hostap: linux $(TARGET_DIR)/usr/bin/hostap_crypt_conf

hostap-clean:
	$(MAKE) DESTDIR=$(TARGET_DIR) CC=$(TARGET_CC) -C $(HOSTAP_DIR) uninstall
	-$(MAKE) -C $(HOSTAP_DIR) clean

hostap-dirclean:
	rm -rf $(HOSTAP_DIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_HOSTAP)),y)
TARGETS+=hostap
endif
