#############################################################
#
# fping
#
#############################################################
#
FPING_SOURCE_URL=http://www.fping.com/download/
FPING_SOURCE=fping.tar.gz
FPING_VER=2.4b2_to
FPING_BUILD_DIR=$(BUILD_DIR)/fping-$(FPING_VER)

$(DL_DIR)/$(FPING_SOURCE):
	$(WGET) -P $(DL_DIR) $(FPING_SOURCE_URL)/$(FPING_SOURCE)

$(FPING_BUILD_DIR)/.unpacked: $(DL_DIR)/$(FPING_SOURCE)
	cd $(BUILD_DIR)
	zcat $(DL_DIR)/$(FPING_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	touch $(FPING_BUILD_DIR)/.unpacked

$(FPING_BUILD_DIR)/.configured: $(FPING_BUILD_DIR)/.unpacked
	(cd $(FPING_BUILD_DIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) \
		CFLAGS="$(TARGET_CFLAGS)" \
		./configure \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=/usr \
		--exec-prefix=/usr \
		$(DISABLE_NLS) \
	);
	touch  $(FPING_BUILD_DIR)/.configured

$(FPING_BUILD_DIR)/fping: $(FPING_BUILD_DIR)/.configured
	$(MAKE) CC=$(TARGET_CC) -C $(FPING_BUILD_DIR)

$(TARGET_DIR)/usr/sbin/fping: $(FPING_BUILD_DIR)/fping
	cp -Rf $(FPING_BUILD_DIR)/fping $(TARGET_DIR)/usr/sbin/
	$(STRIP) $<
	chmod 4750 $(TARGET_DIR)/usr/sbin/fping

fping: $(TARGET_DIR)/usr/sbin/fping

fping-source: $(DL_DIR)/$(FPING_SOURCE)

fping-clean:
	rm -f $(FPING_BUILD_DIR)/*.o $(FPING_BUILD_DIR)/fping

fping-dirclean:
	rm -rf $(FPING_BUILD_DIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_FPING)),y)
TARGETS+=fping
endif
