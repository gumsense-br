#############################################################
#
# ALSA lib
#
#############################################################

ALSA_LIB_VERSION:=1.0.11rc2
ALSA_LIB_SOURCE:=alsa-lib-$(ALSA_LIB_VERSION).tar.bz2
ALSA_LIB_SITE:=ftp://ftp.alsa-project.org/pub/lib/
ALSA_LIB_CAT:=bzcat
ALSA_LIB_DIR:=$(BUILD_DIR)/${shell basename $(ALSA_LIB_SOURCE) .tar.bz2}
ALSA_LIB_BINARY:=libasound.so.2.0.0

$(DL_DIR)/$(ALSA_LIB_SOURCE):
	$(WGET) -P $(DL_DIR) $(ALSA_LIB_SITE)/$(ALSA_LIB_SOURCE)

$(ALSA_LIB_DIR)/.unpacked: $(DL_DIR)/$(ALSA_LIB_SOURCE)
	$(ALSA_LIB_CAT) $(DL_DIR)/$(ALSA_LIB_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	toolchain/patch-kernel.sh $(ALSA_LIB_DIR) package/alsa-lib alsa-lib*.patch*
	touch $(ALSA_LIB_DIR)/.unpacked

# Install ALSA libraries to /usr/lib. 
# TODO: Install configuration files to /etc/alsa instead of /usr/share/alsa.
# TODO: configure says it's not finding softfloat. However, alsa-lib still builds
# just fine, so...
$(ALSA_LIB_DIR)/.configured: $(ALSA_LIB_DIR)/.unpacked
	(cd $(ALSA_LIB_DIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) \
		LDFLAGS="-L$(STAGING_DIR)/lib" \
		CFLAGS="$(TARGET_CFLAGS) -I$(STAGING_DIR)/include" \
		./configure \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=$(STAGING_DIR)/usr \
		--exec-prefix=$(STAGING_DIR)/usr \
		--bindir=$(STAGING_DIR)/usr/bin \
		--sbindir=$(STAGING_DIR)/usr/sbin \
		--libexecdir=$(STAGING_DIR)/usr/lib \
		--sysconfdir=$(STAGING_DIR)/etc \
		--datadir=$(STAGING_DIR)/usr/share \
		--localstatedir=$(STAGING_DIR)/var \
		--mandir=$(STAGING_DIR)/usr/man \
		--infodir=$(STAGING_DIR)/usr/info \
		--with-pic \
		--with-softfloat \
		--disable-static \
		--disable-aload \
		--disable-rawmidi \
		--disable-hwdep \
		--disable-seq \
		--disable-instr \
	);
	touch $(ALSA_LIB_DIR)/.configured

# TODO: check against specific binary.
$(ALSA_LIB_DIR)/src/.libs/$(ALSA_LIB_BINARY): $(ALSA_LIB_DIR)/.configured $(TARGET_CC)
	$(MAKE) CC=$(TARGET_CC) -C $(ALSA_LIB_DIR)

# Install into staging dir, for other packages to build against.
$(STAGING_DIR)/lib/$(ALSA_LIB_BINARY): $(ALSA_LIB_DIR)/src/.libs/$(ALSA_LIB_BINARY)
	$(MAKE) \
	    prefix=$(STAGING_DIR) \
	    exec_prefix=$(STAGING_DIR) \
	    bindir=$(STAGING_DIR)/bin \
	    sbindir=$(STAGING_DIR)/sbin \
	    libexecdir=$(STAGING_DIR)/lib \
	    datadir=$(STAGING_DIR)/usr/share \
	    sysconfdir=$(STAGING_DIR)/etc \
	    localstatedir=$(STAGING_DIR)/var \
	    libdir=$(STAGING_DIR)/lib \
	    infodir=$(STAGING_DIR)/info \
	    mandir=$(STAGING_DIR)/man \
	    includedir=$(STAGING_DIR)/include \
	-C $(ALSA_LIB_DIR) install
# Install into target dir, for utils using shared libraries on target.
$(TARGET_DIR)/lib/$(ALSA_LIB_BINARY): $(STAGING_DIR)/lib/$(ALSA_LIB_BINARY)
	install -D -m 0644 $< $@
	$(STRIP) $@
	(cd $(TARGET_DIR)/lib; \
	   rm -f libasound.so libasound.so.2 libasound.so.2.0; \
	   ln -s libasound.so.2.0.0 libasound.so; \
	   ln -s libasound.so.2.0.0 libasound.so.2.0; \
	   ln -s libasound.so.2.0.0 libasound.so.2)

alsa-lib: uclibc libfloat $(TARGET_DIR)/lib/$(ALSA_LIB_BINARY)

alsa-lib-clean:
	$(MAKE) -C $(ALSA_LIB_DIR) distclean
	rm $(ALSA_LIB_DIR)/.configured

alsa-lib-dirclean:
	rm -rf $(ALSA_LIB_DIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_ALSA_LIB)),y)
TARGETS+=alsa-lib
endif

