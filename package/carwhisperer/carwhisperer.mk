#############################################################
#
# carwhisperer
#
#############################################################
carwhisperer_SOURCE:=carwhisperer-0.1.tar.gz
carwhisperer_SITE:=http://trifinite.org/Downloads
carwhisperer_DIR:=$(BUILD_DIR)/carwhisperer-0.1
carwhisperer_CAT:=gzcat
carwhisperer_BINARY:=carwhisperer
carwhisperer_TARGET_BINARY:=usr/bin/carwhisperer

$(DL_DIR)/$(carwhisperer_SOURCE):
	 $(WGET) -P $(DL_DIR) $(carwhisperer_SITE)/$(carwhisperer_SOURCE)

carwhisperer-source: $(DL_DIR)/$(carwhisperer_SOURCE)

$(carwhisperer_DIR)/configure: $(DL_DIR)/$(carwhisperer_SOURCE)
	$(carwhisperer_CAT) $(DL_DIR)/$(carwhisperer_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	toolchain/patch-kernel.sh $(carwhisperer_DIR) package/carwhisperer carwhisperer-*.patch
	cp package/carwhisperer/carwhisperer/* $(carwhisperer_DIR)

$(carwhisperer_DIR)/Makefile: $(carwhisperer_DIR)/configure
	(cd $(carwhisperer_DIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) \
		CFLAGS="$(TARGET_CFLAGS)" \
		./configure \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=$(TARGET_DIR) \
		--exec-prefix=/usr \
		--bindir=/usr/bin \
		--sbindir=/usr/sbin \
		--libexecdir=/usr/lib \
		--sysconfdir=/etc \
		--datadir=/usr/share/misc \
		--localstatedir=/var \
		--mandir=/usr/man \
		--infodir=/usr/info \
	);

$(carwhisperer_DIR)/$(carwhisperer_BINARY): $(carwhisperer_DIR)/Makefile
	$(MAKE) -C $(carwhisperer_DIR)



#install:
#	cp carwhisperer	/usr/bin/
#	cp /etc/bluetooth/hcid.conf /etc/bluetooth/hcid.conf.old
#	cp hcid.conf /etc/bluetooth/hcid.conf
#	cp cw_pin.pl /usr/bin/
#	cp cw_scanner /usr/bin/

#uninstall:
#	rm /usr/bin/carwhisperer
#	mv /etc/bluetooth/hcid.conf.old /etc/bluetooth/hcid.conf
#	rm /usr/bin/cw_pin.pl 
#	rm /usr/bin/cw_scanner 

$(TARGET_DIR)/$(carwhisperer_TARGET_BINARY): $(carwhisperer_DIR)/$(carwhisperer_BINARY)
	cp $(carwhisperer_DIR)/$(carwhisperer_BINARY) $(TARGET_DIR)/$(carwhisperer_TARGET_BINARY)
	cp $(TARGET_DIR)/etc/bluetooth/hcid.conf $(TARGET_DIR)/etc/bluetooth/hcid.conf.pre.carwhisperer
	cp $(carwhisperer_DIR)/hcid.conf $(TARGET_DIR)/etc/bluetooth/hcid.conf
	cp $(carwhisperer_DIR)/cw_pin.pl $(TARGET_DIR)/usr/bin/
	cp $(carwhisperer_DIR)/cw_scanner $(TARGET_DIR)/usr/bin/
	cp $(carwhisperer_DIR)/samples/message.raw $(TARGET_DIR)/usr/bin/
	chmod 0755 $(TARGET_DIR)/$(carwhisperer_TARGET_BINARY)
	$(STRIP) $@

carwhisperer: uclibc $(TARGET_DIR)/$(carwhisperer_TARGET_BINARY)

carwhisperer-clean:
	rm $(TARGET_DIR)/$(carwhisperer_TARGET_BINARY)
	-$(MAKE) -C $(carwhisperer_DIR) clean

carwhisperer-dirclean:
	rm -rf $(carwhisperer_DIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_CARWHISPERER)),y)
TARGETS+=carwhisperer
endif
