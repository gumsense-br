#############################################################
#
# uptimed
#
#############################################################
UPTIMED_SOURCE:=uptimed-0.3.3.tar.bz2
UPTIMED_SITE:=http://www.rungie.com/files/
UPTIMED_DIR:=$(BUILD_DIR)/uptimed-0.3.3
UPTIMED_CAT:=bzcat
UPTIMED_BINARY:=uptimed
UPRECORDS_BINARY:=uprecords
UPTIME_INIT_SCRIPT:=S99uptimed

$(DL_DIR)/$(UPTIMED_SOURCE):
	 $(WGET) -P $(DL_DIR) $(UPTIMED_SITE)/$(UPTIMED_SOURCE)

uptimed-source: $(DL_DIR)/$(UPTIMED_SOURCE)

$(UPTIMED_DIR)/.unpacked: $(DL_DIR)/$(UPTIMED_SOURCE)
	$(UPTIMED_CAT) $(DL_DIR)/$(UPTIMED_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	touch  $(UPTIMED_DIR)/.unpacked

$(UPTIMED_DIR)/.configured: $(UPTIMED_DIR)/.unpacked
	(cd $(UPTIMED_DIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) \
		CFLAGS="$(TARGET_CFLAGS)" \
		./configure \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=/usr \
		--exec-prefix=/usr \
		--bindir=/usr/bin \
		--sbindir=/usr/sbin \
		--libexecdir=/usr/lib \
		--sysconfdir=/etc \
		--datadir=/usr/share/misc \
		--localstatedir=/var \
		--mandir=/usr/man \
		--infodir=/usr/info \
		--disable-shared \
		--enable-static \
	);
	touch  $(UPTIMED_DIR)/.configured

$(UPTIMED_DIR)/src/$(UPTIMED_BINARY): $(UPTIMED_DIR)/.configured
	$(MAKE) -C $(UPTIMED_DIR)

$(TARGET_DIR)/usr/sbin/$(UPTIMED_BINARY): $(UPTIMED_DIR)/src/$(UPTIMED_BINARY)
	mkdir -p $(TARGET_DIR)/usr/sbin $(TARGET_DIR)/var/spool/uptimed
	install -m 0700 $< $@
	$(STRIP) $@

$(TARGET_DIR)/usr/bin/$(UPRECORDS_BINARY): $(UPTIMED_DIR)/src/$(UPTIMED_BINARY)
	mkdir -p $(TARGET_DIR)/usr/bin
	install -m 0700 $(UPTIMED_DIR)/src/$(UPRECORDS_BINARY) $@
	$(STRIP) $@

$(TARGET_DIR)/etc/init.d/$(UPTIME_INIT_SCRIPT): package/uptimed/S99uptimed
	mkdir -p /etc/init.d
	install -m 0644 $(UPTIMED_DIR)/etc/uptimed.conf-dist $(TARGET_DIR)/etc/uptimed.conf
	install -m 0755 $< $@

uptimed: uclibc $(TARGET_DIR)/usr/sbin/$(UPTIMED_BINARY) $(TARGET_DIR)/usr/bin/$(UPRECORDS_BINARY) $(TARGET_DIR)/etc/init.d/$(UPTIME_INIT_SCRIPT)

uptimed-clean:
	-$(MAKE) -C $(UPTIMED_DIR) clean

uptimed-dirclean:
	rm -rf $(UPTIMED_DIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_UPTIMED)),y)
TARGETS+=uptimed
endif
