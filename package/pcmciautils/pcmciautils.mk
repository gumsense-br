#############################################################
#
# pcmciautils for udev based pcmcia control
#
#############################################################
# Copyright (C) 2001-2003 by Erik Andersen <andersen@codepoet.org>
# Copyright (C) 2002 by Tim Riker <Tim@Rikers.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Library General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
# USA

PCMCIAUTILS_VERSION:=014
PCMCIAUTILS_SOURCE:=pcmciautils-$(PCMCIAUTILS_VERSION).tar.bz2
PCMCIAUTILS_SITE:=http://www.kernel.org/pub/linux/utils/kernel/pcmcia
PCMCIAUTILS_DIR:=$(BUILD_DIR)/pcmciautils-$(PCMCIAUTILS_VERSION)
PCMCIAUTILS_CAT:=bzcat

$(DL_DIR)/$(PCMCIAUTILS_SOURCE):
	$(WGET) -P $(DL_DIR) $(PCMCIAUTILS_SITE)/$(PCMCIAUTILS_SOURCE)

pcmciautils-source: $(DL_DIR)/$(PCMCIAUTILS_SOURCE)

$(PCMCIAUTILS_DIR)/Makefile: $(DL_DIR)/$(PCMCIAUTILS_SOURCE)
	$(PCMCIAUTILS_CAT) $(DL_DIR)/$(PCMCIAUTILS_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	toolchain/patch-kernel.sh $(PCMCIAUTILS_DIR) package/pcmciautils pcmciautils-*.patch
	touch $@

$(TARGET_DIR)/etc/udev/rules.d/60-pcmcia.rules: $(PCMCIAUTILS_DIR)/Makefile
	$(MAKE) -C $(PCMCIAUTILS_DIR) V=true SYMLINK="ln -sf" CROSS="$(KERNEL_CROSS)" DESTDIR="$(TARGET_DIR)" STARTUP="false" UDEV="true" all
	$(MAKE) -C $(PCMCIAUTILS_DIR) V=true SYMLINK="ln -sf" CROSS="$(KERNEL_CROSS)" DESTDIR="$(TARGET_DIR)" STARTUP="false" UDEV="true" install-udev install-tools

pcmciautils: udev libsysfs $(TARGET_DIR)/etc/udev/rules.d/60-pcmcia.rules

pcmciautils-clean:
	rm $(TARGET_DIR)/etc/udev/rules.d/60-pcmcia-rules

pcmciautils-dirclean:
	rm -rf $(PCMCIAUTILS_DIR)
#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_PCMCIAUTILS)),y)
TARGETS+=pcmciautils
endif
