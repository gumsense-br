#############################################################
#
# iconv
#
#############################################################
ICONV_VERSION=1.10
ICONV_SOVER=2.3.0
ICONV_SOURCE=libiconv-$(ICONV_VERSION).tar.gz
ICONV_SITE=http://ftp.gnu.org/pub/gnu/libiconv
ICONV_DIR=$(BUILD_DIR)/libiconv-$(ICONV_VERSION)

$(DL_DIR)/$(ICONV_SOURCE):
	$(WGET) -P $(DL_DIR) $(ICONV_SITE)/$(ICONV_SOURCE)

$(ICONV_DIR)/.source: $(DL_DIR)/$(ICONV_SOURCE)
	zcat $(DL_DIR)/$(ICONV_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	touch $(ICONV_DIR)/.source

$(ICONV_DIR)/.configured: $(ICONV_DIR)/.source
	(cd $(ICONV_DIR); \
		$(TARGET_CONFIGURE_OPTS) \
		CFLAGS="$(TARGET_CFLAGS)" \
		./configure \
		-target=$(GNU_TARGET_NAME) \
		-host=$(GNU_TARGET_NAME) \
		-build=$(GNU_HOST_NAME) \
		--prefix=/usr \
		--exec-prefix=/usr \
		--bindir=/usr/bin \
		--sbindir=/usr/sbin \
		--libexecdir=/usr/lib \
		--sysconfdir=/etc \
		--datadir=/usr/share \
		--localstatedir=/var \
		--mandir=/usr/man \
		--infodir=/usr/info \
		--with-gnu-ld \
		--enable-shared \
		--enable-static \
		--disable-nls \
	);
	touch $(ICONV_DIR)/.configured;

$(ICONV_DIR)/include/iconv.h: $(ICONV_DIR)/.configured

$(ICONV_DIR)/lib/.libs/libiconv.so: $(ICONV_DIR)/.configured
	$(MAKE) -C $(ICONV_DIR)

$(TARGET_DIR)/usr/lib/libiconv.so.$(ICONV_SOVER): $(ICONV_DIR)/lib/.libs/libiconv.so.$(ICONV_SOVER)
	cp -dpf $< $@
	$(STRIP) $@
	(cd $(TARGET_DIR)/usr/lib; ln -fs libiconv.so.$(ICONV_SOVER) libiconv.so; ln -fs libiconv.so.$(ICONV_SOVER) libiconv.so.2);
	chmod a-x $@

$(STAGING_DIR)/include/iconv.h: $(ICONV_DIR)/include/iconv.h
	cp -dpf $(ICONV_DIR)/include/iconv.h $(STAGING_DIR)/include

$(STAGING_DIR)/lib/libiconv.a: $(ICONV_DIR)/lib/.libs/libiconv.so
	cp -dpf $($ICONV_DIR)/include/iconv.h $(STAGING_DIR)/include
	cp -dpf $(ICONV_DIR)/lib/.libs/libiconv.a $(STAGING_DIR)/lib/

iconv-headers: $(STAGING_DIR)/include/iconv.h $(STAGING_DIR)/lib/libiconv.a

iconv: uclibc $(STAGING_DIR)/include/iconv.h $(TARGET_DIR)/usr/lib/libiconv.so.$(ICONV_SOVER)

iconv-source: $(DL_DIR)/$(ICONV_SOURCE)

iconv-clean:
	rm -f $(TARGET_DIR)/lib/libiconv.so* $(STAGING_DIR)/include/iconv.h $(STAGING_DIR)/lib/libiconv.a
	-$(MAKE) -C $(ICONV_DIR) clean

iconv-dirclean:
	rm -rf $(ICONV_DIR)
#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_ICONV)),y)
TARGETS+=iconv
endif
