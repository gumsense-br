#############################################################
#
# gdbm
#
#############################################################
GDBM_SOURCE:=gdbm-1.8.3.tar.gz
GDBM_SITE:=ftp://ftp.gnu.org/gnu/gdbm
GDBM_DIR:=$(BUILD_DIR)/gdbm-1.8.3
GDBM_CAT:=zcat
GDBM_BINARY:=.libs/libgdbm.so.3.0.0
GDBM_TARGET_BINARY:=usr/lib/libgdbm.so.3.0.0

$(DL_DIR)/$(GDBM_SOURCE):
	 $(WGET) -P $(DL_DIR) $(GDBM_SITE)/$(GDBM_SOURCE)

gdbm-source: $(DL_DIR)/$(GDBM_SOURCE)

$(GDBM_DIR)/.unpacked: $(DL_DIR)/$(GDBM_SOURCE)
	$(GDBM_CAT) $(DL_DIR)/$(GDBM_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	touch $(GDBM_DIR)/.unpacked

$(GDBM_DIR)/.configured: $(GDBM_DIR)/.unpacked
	(cd $(GDBM_DIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) \
		CFLAGS="$(TARGET_CFLAGS)" \
		./configure \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=/usr \
		--exec-prefix=/usr \
		--bindir=/usr/bin \
		--sbindir=/usr/sbin \
		--libexecdir=/usr/lib \
		--sysconfdir=/etc \
		--datadir=/usr/share \
		--localstatedir=/var \
		--mandir=/usr/man \
		--infodir=/usr/info \
	);
	touch  $(GDBM_DIR)/.configured

$(GDBM_DIR)/$(GDBM_BINARY): $(GDBM_DIR)/.configured
	$(MAKE) $(JLEVEL) CC=$(TARGET_CC) -C $(GDBM_DIR)

$(TARGET_DIR)/$(GDBM_TARGET_BINARY): $(GDBM_DIR)/$(GDBM_BINARY)
	cp -dpf $(GDBM_DIR)/gdbm.h $(STAGING_DIR)/include/gdbm.h
	cp -dpf $(GDBM_DIR)/.libs/libgdbm.so* $(STAGING_DIR)/lib
	cp -dpf $(GDBM_DIR)/.libs/libgdbm.so* $(TARGET_DIR)/usr/lib

gdbm: uclibc $(TARGET_DIR)/$(GDBM_TARGET_BINARY)

gdbm-clean:
	$(MAKE) DESTDIR=$(TARGET_DIR) CC=$(TARGET_CC) -C $(GDBM_DIR) uninstall
	-$(MAKE) -C $(GDBM_DIR) clean

gdbm-dirclean:
	rm -rf $(GDBM_DIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_GDBM)),y)
TARGETS+=gdbm
endif
