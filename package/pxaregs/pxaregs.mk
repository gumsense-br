#############################################################
#
# pxaregs originally from http://www.mn-logistik.de/unsupported/pxa250/
#
#############################################################
PXAREGS_SOURCE:=pxaregs-1.14.tar.bz
PXAREGS_SITE:=http://files.gumstix.com
PXAREGS_DIR:=$(BUILD_DIR)/pxaregs-1.14
PXAREGS_CAT:=bzcat
PXAREGS_BINARY:=pxaregs
PXAREGS_TARGET_BINARY:=usr/sbin/pxaregs

$(DL_DIR)/$(PXAREGS_SOURCE):
	 $(WGET) -P $(DL_DIR) $(PXAREGS_SITE)/$(PXAREGS_SOURCE)

bison-source: $(DL_DIR)/$(PXAREGS_SOURCE)

$(PXAREGS_DIR)/pxaregs.c: $(DL_DIR)/$(PXAREGS_SOURCE)
	$(PXAREGS_CAT) $(DL_DIR)/$(PXAREGS_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	toolchain/patch-kernel.sh $(PXAREGS_DIR) package/pxaregs pxaregs-*.patch

$(PXAREGS_DIR)/$(PXAREGS_BINARY): $(PXAREGS_DIR)/pxaregs.c
	$(TARGET_CC) $(TARGET_CFLAGS) -o $@ $<

$(TARGET_DIR)/$(PXAREGS_TARGET_BINARY): $(PXAREGS_DIR)/$(PXAREGS_BINARY)
	$(STRIP) $(PXAREGS_DIR)/$(PXAREGS_BINARY)
	install -m 0755 $< $@

pxaregs: uclibc $(TARGET_DIR)/$(PXAREGS_TARGET_BINARY)

pxaregs-clean:
	rm $(TARGET_DIR)/$(PXAREGS_TARGET_BINARY)
	rm $(PXAREGS_DIR)/$(PXAREGS_BINARY)

pxaregs-dirclean:
	rm -rf $(PXAREGS_DIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_PXAREGS)),y)
TARGETS+=pxaregs
endif
