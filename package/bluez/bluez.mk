#############################################################
#
# bluezutils - User Space Program For Controling Bridging
#
#############################################################
#
BLUEZ_SOURCE_URL=http://bluez.sourceforge.net/download
BLUEZ_LIBS_SOURCE=bluez-libs-2.24.tar.gz
BLUEZ_UTILS_SOURCE=bluez-utils-2.24.tar.gz
BLUEZ_HCIDUMP_SOURCE=bluez-hcidump-1.29.tar.gz
BLUEZ_LIBS_BUILD_DIR=$(BUILD_DIR)/bluez-libs-2.24
BLUEZ_UTILS_BUILD_DIR=$(BUILD_DIR)/bluez-utils-2.24
BLUEZ_HCIDUMP_BUILD_DIR=$(BUILD_DIR)/bluez-hcidump-1.29

$(DL_DIR)/$(BLUEZ_LIBS_SOURCE):
	$(WGET) -P $(DL_DIR) $(BLUEZ_SOURCE_URL)/$(BLUEZ_LIBS_SOURCE)

$(DL_DIR)/$(BLUEZ_UTILS_SOURCE):
	$(WGET) -P $(DL_DIR) $(BLUEZ_SOURCE_URL)/$(BLUEZ_UTILS_SOURCE)

$(DL_DIR)/$(BLUEZ_HCIDUMP_SOURCE):
	$(WGET) -P $(DL_DIR) $(BLUEZ_SOURCE_URL)/$(BLUEZ_HCIDUMP_SOURCE)

$(BLUEZ_LIBS_BUILD_DIR)/.unpacked: $(DL_DIR)/$(BLUEZ_LIBS_SOURCE)
	zcat $(DL_DIR)/$(BLUEZ_LIBS_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	toolchain/patch-kernel.sh $(BLUEZ_LIBS_BUILD_DIR) package/bluez bluez-libs*.patch
	touch $(BLUEZ_LIBS_BUILD_DIR)/.unpacked

$(BLUEZ_UTILS_BUILD_DIR)/.unpacked: $(DL_DIR)/$(BLUEZ_UTILS_SOURCE)
	zcat $(DL_DIR)/$(BLUEZ_UTILS_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	toolchain/patch-kernel.sh $(BLUEZ_UTILS_BUILD_DIR) package/bluez bluez-utils*.patch
	touch $(BLUEZ_UTILS_BUILD_DIR)/.unpacked

$(BLUEZ_HCIDUMP_BUILD_DIR)/.unpacked: $(DL_DIR)/$(BLUEZ_HCIDUMP_SOURCE)
	zcat $(DL_DIR)/$(BLUEZ_HCIDUMP_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	touch $(BLUEZ_HCIDUMP_BUILD_DIR)/.unpacked

$(BLUEZ_LIBS_BUILD_DIR)/.configured: $(BLUEZ_LIBS_BUILD_DIR)/.unpacked
	(cd $(BLUEZ_LIBS_BUILD_DIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) CFLAGS="$(TARGET_CFLAGS)" \
		./configure \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=/usr \
		--exec-prefix=/usr \
		--bindir=/usr/bin \
		--sbindir=/usr/sbin \
		--libexecdir=/usr/lib \
		--sysconfdir=/etc \
		--datadir=/usr/share \
		--localstatedir=/var \
		--mandir=/usr/man \
		--infodir=/usr/info );
	touch  $(BLUEZ_LIBS_BUILD_DIR)/.configured

ifeq ($(strip $(BR2_PACKAGE_ALSA-LIB)),y)
WITH_ALSA=--with-alsa=$(STAGING_DIR)
else
WITH_ALSA=--without-alsa
endif

ifeq ($(strip $(BR2_PACKAGE_OPENOBEX)),y)
OBEX_DEP=$(STAGING_DIR)/lib/libopenobex.so
WITH_OBEX=--with-openobex=$(STAGING_DIR)
else
OBEX_DEP=
WITH_OBEX=--without-obex
endif

$(BLUEZ_UTILS_BUILD_DIR)/.configured: $(BLUEZ_UTILS_BUILD_DIR)/.unpacked $(OBEX_DEP) $(STAGING_DIR)/include/bluetooth/bluetooth.h
	(cd $(BLUEZ_UTILS_BUILD_DIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) CFLAGS="$(TARGET_CFLAGS)" \
		./configure \
		--without-cups \
		--without-usb \
		--without-dbus \
		--without-fuse \
		--enable-test \
		--with-bluez=$(STAGING_DIR) \
		$(WITH_OBEX) \
		$(WITH_ALSA) \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=/usr \
		--exec-prefix=/usr \
		--bindir=/usr/bin \
		--sbindir=/usr/sbin \
		--libexecdir=/usr/lib \
		--sysconfdir=/etc \
		--datadir=/usr/share \
		--localstatedir=/var \
		--mandir=/usr/man \
		--infodir=/usr/info );
	touch  $(BLUEZ_UTILS_BUILD_DIR)/.configured


$(BLUEZ_HCIDUMP_BUILD_DIR)/.configured: $(BLUEZ_HCIDUMP_BUILD_DIR)/.unpacked $(STAGING_DIR)/include/bluetooth/bluetooth.h
	(cd $(BLUEZ_HCIDUMP_BUILD_DIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) CFLAGS="$(TARGET_CFLAGS)" \
		./configure \
		--with-bluez=$(STAGING_DIR) \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=/usr \
		--exec-prefix=/usr \
		--bindir=/usr/bin \
		--sbindir=/usr/sbin \
		--libexecdir=/usr/lib \
		--sysconfdir=/etc \
		--datadir=/usr/share \
		--localstatedir=/var \
		--mandir=/usr/man \
		--infodir=/usr/info );
	touch  $(BLUEZ_HCIDUMP_BUILD_DIR)/.configured

$(BLUEZ_LIBS_BUILD_DIR)/src/libbluetooth.la: $(BLUEZ_LIBS_BUILD_DIR)/.configured
	$(MAKE) -C $(BLUEZ_LIBS_BUILD_DIR)

$(BLUEZ_UTILS_BUILD_DIR)/tools/hciattach: $(BLUEZ_UTILS_BUILD_DIR)/.configured
	$(MAKE) -C $(BLUEZ_UTILS_BUILD_DIR)
	touch $@

$(BLUEZ_HCIDUMP_BUILD_DIR)/hcidump: $(BLUEZ_HCIDUMP_BUILD_DIR)/.configured
	$(MAKE) -C $(BLUEZ_HCIDUMP_BUILD_DIR)

$(STAGING_DIR)/include/bluetooth/bluetooth.h: $(BLUEZ_LIBS_BUILD_DIR)/.unpacked
	mkdir -p $(STAGING_DIR)/include/bluetooth
	cp -f $(BLUEZ_LIBS_BUILD_DIR)/include/*.h $(STAGING_DIR)/include/bluetooth

$(STAGING_DIR)/lib/libbluetooth.so $(STAGING_DIR)/lib/libbluetooth.a: $(BLUEZ_LIBS_BUILD_DIR)/src/libbluetooth.la
	/bin/sh $(BLUEZ_LIBS_BUILD_DIR)/libtool --mode=install install -c $(BLUEZ_LIBS_BUILD_DIR)/src/libbluetooth.la $(STAGING_DIR)/lib/libbluetooth.la
	$(SED) "\$$s:=.*:='$(STAGING_DIR)/lib':" $(STAGING_DIR)/lib/libbluetooth.la

$(TARGET_DIR)/usr/lib/libbluetooth.so: $(STAGING_DIR)/lib/libbluetooth.so
	mkdir -p $(TARGET_DIR)/usr/lib
	install -D -m 0755 $(STAGING_DIR)/lib/libbluetooth.so* $(TARGET_DIR)/usr/lib
	$(SAFE_STRIP) $(TARGET_DIR)/usr/lib/libbluetooth.so

$(TARGET_DIR)/usr/sbin/hcidump: $(BLUEZ_HCIDUMP_BUILD_DIR)/hcidump
	install -D -m 0755 $(BLUEZ_HCIDUMP_BUILD_DIR)/src/hcidump $(TARGET_DIR)/usr/sbin/hcidump
	$(STRIP) $(TARGET_DIR)/usr/sbin/hcidump

$(TARGET_DIR)/usr/sbin/hciattach: $(BLUEZ_UTILS_BUILD_DIR)/tools/hciattach
	install -D -m 0755 $(BLUEZ_UTILS_BUILD_DIR)/hcid/hcid $(TARGET_DIR)/usr/sbin/hcid
	$(STRIP) $(TARGET_DIR)/usr/sbin/hcid
	mkdir -p $(TARGET_DIR)/etc/bluetooth/pan $(TARGET_DIR)/etc/bluetooth/rfcomm
	install -D -m 0644 $(BLUEZ_UTILS_BUILD_DIR)/hcid/hcid.conf $(TARGET_DIR)/etc/bluetooth/hcid.conf
	install -D -m 0755 package/bluez/bluez-utils-bluepin.sh $(TARGET_DIR)/etc/bluetooth/bluepin
	install -D -m 0755 package/bluez/bluez-pand-devup.sh $(TARGET_DIR)/etc/bluetooth/pan/dev-up
	install -D -m 0755 package/bluez/rfcomm-listen $(TARGET_DIR)/etc/bluetooth/rfcomm/rfcomm-listen
	install -D -m 0755 package/bluez/rfcomm-getty $(TARGET_DIR)/etc/bluetooth/rfcomm/rfcomm-getty
	echo "1234" > $(TARGET_DIR)/etc/bluetooth/pin
	chmod 0600 $(TARGET_DIR)/etc/bluetooth/pin
	(list='hcitool l2ping sdptool ciptool'; for p in $$list; do \
     		install -D -m 0755 $(BLUEZ_UTILS_BUILD_DIR)/tools/$$p $(TARGET_DIR)/usr/bin/$$p; \
	done)
	(list='hciattach hciconfig'; for p in $$list; do \
		install -D -m 0755 $(BLUEZ_UTILS_BUILD_DIR)/tools/$$p $(TARGET_DIR)/usr/sbin/$$p; \
	done)
	install -D -m 0755 $(BLUEZ_UTILS_BUILD_DIR)/rfcomm/rfcomm $(TARGET_DIR)/usr/sbin/rfcomm
	$(STRIP) $(TARGET_DIR)/usr/sbin/rfcomm
	install -D -m 0644 $(BLUEZ_UTILS_BUILD_DIR)/rfcomm/rfcomm.conf $(TARGET_DIR)/etc/bluetooth/rfcomm.conf
	install -D -m 0755 $(BLUEZ_UTILS_BUILD_DIR)/sdpd/sdpd $(TARGET_DIR)/usr/sbin/sdpd
	$(STRIP) $(TARGET_DIR)/usr/sbin/sdpd
	install -D -m 0755 $(BLUEZ_UTILS_BUILD_DIR)/dund/dund $(TARGET_DIR)/usr/bin/dund
	$(STRIP) $(TARGET_DIR)/usr/bin/dund
	install -D -m 0755 $(BLUEZ_UTILS_BUILD_DIR)/pand/pand $(TARGET_DIR)/usr/bin/pand
	$(STRIP) $(TARGET_DIR)/usr/bin/pand
	install -D -m 0755 $(BLUEZ_UTILS_BUILD_DIR)/hidd/hidd $(TARGET_DIR)/usr/bin/hidd
	$(STRIP) $(TARGET_DIR)/usr/bin/hidd
	install -D -m 0755 $(BLUEZ_UTILS_BUILD_DIR)/scripts/bluetooth.init $(TARGET_DIR)/etc/init.d/S30bluetooth
	mkdir -p $(TARGET_DIR)/etc/default
	install -D -m 0644 $(BLUEZ_UTILS_BUILD_DIR)/scripts/bluetooth.default $(TARGET_DIR)/etc/default/bluetooth

.PHONY: bluez-libs-staging bluez-libs bluez-libs-source bluez-utils

bluez-utils: bluez-libs $(TARGET_DIR)/usr/sbin/hciattach pxaregs

bluez-hcidump: bluez-libs $(TARGET_DIR)/usr/sbin/hcidump

bluez-libs-staging: $(STAGING_DIR)/lib/libbluetooth.a $(STAGING_DIR)/include/bluetooth/bluetooth.h

bluez-libs: $(TARGET_DIR)/usr/lib/libbluetooth.so

bluez-libs-source: $(DL_DIR)/$(BLUEZ_SOURCE)

bluez-libs-clean:
	-$(MAKE) -C $(BLUEZ_LIBS_BUILD_DIR) clean

bluez-libs-dirclean:
	rm -rf $(BLUEZ_LIBS_BUILD_DIR)

#############################################################
#
## Toplevel Makefile options
#
##############################################################
ifeq ($(strip $(BR2_PACKAGE_BLUEZ)),y)
TARGETS+=bluez-utils
endif
