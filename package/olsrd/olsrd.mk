#############################################################
#
# olrsd - Optimized Link State Routing protocol
#
#############################################################
OLSRD_VERSION:=0.4.10
OLSRD_SOURCE:=olsrd-$(OLSRD_VERSION).tar.gz
OLSRD_SITE:=http://www.olsr.org/releases/0.4
OLSRD_DIR:=$(BUILD_DIR)/olsrd-$(OLSRD_VERSION)
OLSRD_BINARY:=olsrd
OLSRD_TARGET_BINARY:=usr/sbin/olsrd

$(DL_DIR)/$(OLSRD_SOURCE):
	$(WGET) -P $(DL_DIR) $(OLSRD_SITE)/$(OLSRD_SOURCE)

$(OLSRD_DIR)/.source: $(DL_DIR)/$(OLSRD_SOURCE)
	zcat $(DL_DIR)/$(OLSRD_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	touch $(OLSRD_DIR)/.source

$(OLSRD_DIR)/$(OLSRD_BINARY): $(OLSRD_DIR)/.source
	CC="$(TARGET_CC)" STRIP="$(STRIP)" make -C $(OLSRD_DIR)

$(OLSRD_DIR)/lib/dot_draw/src/.patched:
ifeq ($(strip $(BR2_PACKAGE_OLSRD_DD_ANYHOST)),y)
	toolchain/patch-kernel.sh $(OLSRD_DIR)/lib/dot_draw/src package/olsrd dot_draw_anyhost.patch
	touch $(OLSRD_DIR)/lib/dot_draw/src/.patched
endif

olsrd-plugins: $(OLSRD_DIR)/.source $(OLSRD_DIR)/lib/dot_draw/src/.patched
ifeq ($(strip $(BR2_PACKAGE_OLSRD_DOT_DRAW)),y)
	CC="$(TARGET_CC)" STRIP="$(STRIP)" make -C $(OLSRD_DIR)/lib/dot_draw
endif

olsrd-plugins-installed: olsrd-plugins
ifeq ($(strip $(BR2_PACKAGE_OLSRD_DOT_DRAW)),y)
	cp -f $(OLSRD_DIR)/lib/dot_draw/olsrd_dot_draw.so.0.3 $(TARGET_DIR)/usr/lib
endif

$(TARGET_DIR)/$(OLSRD_TARGET_BINARY): $(OLSRD_DIR)/$(OLSRD_BINARY)
	INSTALL_PREFIX="$(TARGET_DIR)" CC="$(TARGET_CC)" STRIP="$(STRIP)" make -C $(OLSRD_DIR) install
	cp -f package/olsrd/s80olsrd $(TARGET_DIR)/etc/init.d/
	rm -f $(TARGET_DIR)/usr/share/man/man5/olsrd.conf.5.gz
	rm -f $(TARGET_DIR)/usr/share/man/man8/olsrd.8.gz

olsrd: uclibc olsrd-plugins-installed $(TARGET_DIR)/$(OLSRD_TARGET_BINARY)

olsrd-source: $(DL_DIR)/$(OLSRD_SOURCE)

olsrd-clean:
	rm -f $(TARGET_DIR)/usr/sbin/olsrd
	rm -f $(TARGET_DIR)/etc/olsrd.conf
	$(MAKE) -C $(OLSRD_DIR) uberclean

olsrd-dirclean:
	rm -rf $(OLSRD_DIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_OLSRD)),y)
TARGETS+=olsrd
endif
