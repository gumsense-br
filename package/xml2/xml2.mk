#############################################################
#
# libxml2
#
#############################################################
XML2_VERSION=2.6.19
XML2_SOURCE=libxml2-$(XML2_VERSION).tar.gz
XML2_SITE=ftp://xmlsoft.org
XML2_DIR=$(BUILD_DIR)/libxml2-$(XML2_VERSION)

$(DL_DIR)/$(XML2_SOURCE):
	$(WGET) -P $(DL_DIR) $(XML2_SITE)/$(XML2_SOURCE)

$(XML2_DIR)/.source: $(DL_DIR)/$(XML2_SOURCE)
	gzip -d -c $(DL_DIR)/$(XML2_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	touch $(XML2_DIR)/.source

$(XML2_DIR)/.configured: $(XML2_DIR)/.source
	(cd $(XML2_DIR); \
		$(TARGET_CONFIGURE_OPTS) \
		CFLAGS="$(TARGET_CFLAGS) -DNO_LARGEFILE_SOURCE" \
		./configure \
		--prefix=/usr \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=/usr \
		--exec-prefix=/usr \
		--bindir=/usr/bin \
		--sbindir=/usr/sbin \
		--libexecdir=/usr/lib \
		--sysconfdir=/etc \
		--datadir=/usr/share \
		--localstatedir=/var \
		--mandir=/usr/man \
		--infodir=/usr/info \
		--with-gnu-ld \
		--enable-shared \
		--enable-static \
		--enable-ipv6=no \
		--without-debugging \
		--without-python \
	);
	touch $(XML2_DIR)/.configured;

$(XML2_DIR)/libxml2.so: $(XML2_DIR)/.configured
	$(MAKE) -C $(XML2_DIR)
	
#$(XML2_DIR)/libxml2.so: $(XML2_DIR)/.configured
#	$(MAKE) CC=$(TARGET_CC) TAG=CXX AR="$(TARGET_CROSS)ar rc" RANLIB=$(TARGET_CROSS)ranlib -C $(XML2_DIR)

$(STAGING_DIR)/lib/libxml2.so: $(XML2_DIR)/libxml2.so
	cp -dpf $(XML2_DIR)/.libs/libxml2.a $(STAGING_DIR)/lib;
	mkdir -p $(STAGING_DIR)/include/libxml;
	cp -dpf $(XML2_DIR)/include/libxml/*.h $(STAGING_DIR)/include/libxml;
	cp -dpf $(XML2_DIR)/.libs/libxml2.so.$(XML2_VERSION) $(STAGING_DIR)/lib;
	(cd $(STAGING_DIR)/lib; ln -fs libxml2.so.$(XML2_VERSION) libxml2.so; ln -fs libxml2.so.$(XML2_VERSION) libxml2.so.2);
	chmod a-x $(STAGING_DIR)/lib/libxml2.so.$(XML2_VERSION)
	touch -c $(STAGING_DIR)/lib/libxml2.so.$(XML2_VERSION)

$(TARGET_DIR)/lib/libxml2.so: $(STAGING_DIR)/lib/libxml2.so
	cp -dpf $(STAGING_DIR)/lib/libxml2.so* $(TARGET_DIR)/lib;
	-$(STRIP) $(TARGET_DIR)/lib/libxml2.so*
	touch -c $(TARGET_DIR)/lib/libxml2.so.$(XML2_VERSION)

$(TARGET_DIR)/usr/lib/libxml2.a: $(STAGING_DIR)/lib/libxml2.so
	mkdir -p $(TARGET_DIR)/usr/include/libxml
	cp -dpf $(STAGING_DIR)/include/*.h $(TARGET_DIR)/usr/include/libxml
	cp -dpf $(STAGING_DIR)/lib/libxml2.a $(TARGET_DIR)/usr/lib/
	rm -f $(TARGET_DIR)/lib/libxml2.so.$(XML2_VERSION)
	(cd $(TARGET_DIR)/usr/lib; ln -fs ../../lib/libxml2.so.$(XML2_VERSION) libxml2.so)
	touch -c $(TARGET_DIR)/usr/lib/libxml2.a

xml2-headers: $(TARGET_DIR)/usr/lib/libxml2.a

xml2: uclibc $(TARGET_DIR)/lib/libxml2.so

xml2-source: $(DL_DIR)/$(XML2_SOURCE)

xml2-clean:
	rm -f $(TARGET_DIR)/lib/libxml2.so*
	-$(MAKE) -C $(XML2_DIR) clean

xml2-dirclean:
	rm -rf $(XML2_DIR)
#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_XML2)),y)
TARGETS+=xml2
endif
