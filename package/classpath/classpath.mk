#############################################################
#
# gnu classpath
#
#############################################################
CLASSPATH_SOURCE:=classpath-0.90.tar.gz
CLASSPATH_SITE:=ftp://ftp.gnu.org/gnu/classpath/
CLASSPATH_CAT:=zcat
CLASSPATH_DIR:=$(BUILD_DIR)/classpath-0.90

CLASSPATH_ARCHIVE:=lib/glibj.zip
CLASSPATH_TARGET_ARCHIVE:=usr/share/classpath/glibj.zip

$(DL_DIR)/$(CLASSPATH_SOURCE):
	 $(WGET) -P $(DL_DIR) $(CLASSPATH_SITE)/$(CLASSPATH_SOURCE)

classpath-source: $(DL_DIR)/$(CLASSPATH_SOURCE)

$(CLASSPATH_DIR)/.unpacked: $(DL_DIR)/$(CLASSPATH_SOURCE)
	$(CLASSPATH_CAT) $(DL_DIR)/$(CLASSPATH_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	toolchain/patch-kernel.sh $(CLASSPATH_DIR) package/classpath/ classpath-*.patch
	touch $(CLASSPATH_DIR)/.unpacked

$(CLASSPATH_DIR)/.configured: $(CLASSPATH_DIR)/.unpacked
	(cd $(CLASSPATH_DIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) CC_FOR_BUILD=$(HOSTCC) \
		CFLAGS="$(TARGET_CFLAGS)" \
		./configure \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=/usr \
		--exec-prefix=/usr \
		--bindir=/usr/bin \
		--sbindir=/usr/sbin \
		--libexecdir=/usr/lib \
		--sysconfdir=/etc \
		--datadir=/usr/share \
		--localstatedir=/var \
		--mandir=/usr/man \
		--infodir=/usr/info \
		--disable-gtk-peer \
	);
	touch  $(CLASSPATH_DIR)/.configured


# 
# CLASSPATH_ARCHIVE and CLASSPATH_TARGET_ARCHIVE are used as stand-ins
# for all the files that get installed on the target system.
#

$(CLASSPATH_DIR)/$(CLASSPATH_ARCHIVE): $(CLASSPATH_DIR)/.configured
	@if $(shell which zip) == ''; then echo "zip must be installed on the build system to build classpath"; exit 1; fi
	$(MAKE) CC=$(TARGET_CC) CC_FOR_BUILD=$(HOSTCC) -C $(CLASSPATH_DIR)

$(TARGET_DIR)/$(CLASSPATH_TARGET_ARCHIVE): $(CLASSPATH_DIR)/$(CLASSPATH_ARCHIVE)
	$(MAKE) DESTDIR=$(TARGET_DIR) CC=$(TARGET_CC) -C $(CLASSPATH_DIR) install
	rm -rf $(TARGET_DIR)/usr/share/classpath/examples

classpath: uclibc $(TARGET_DIR)/$(CLASSPATH_TARGET_ARCHIVE)
#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_CLASSPATH)),y)
TARGETS+=classpath
endif
