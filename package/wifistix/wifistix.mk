#############################################################
#
# Driver for wifistix module
#
#############################################################
CF8385_SITE:=http://files.gumstix.com

CF8385_TARGET_CONF=$(BUILD_DIR)/.cf8385
CF8385_TARGET_MODULE=$(TARGET_DIR)/lib/modules/2.6.20-rt5gum/kernel/drivers/net/wireless/mcf25.ko
MARVELL_BINARY_DRIVERS=marvell-binaries-2.6.20-1.tar

$(DL_DIR)/$(MARVELL_BINARY_DRIVERS):
	 $(WGET) -P $(DL_DIR) $(CF8385_SITE)/$(MARVELL_BINARY_DRIVERS)

cf8385-source: $(DL_DIR)/$(MARVELL_BINARY_DRIVERS)

$(CF8385_TARGET_MODULE): $(DL_DIR)/$(MARVELL_BINARY_DRIVERS)
	tar -C $(TARGET_DIR) $(TAR_OPTIONS) $(DL_DIR)/$(MARVELL_BINARY_DRIVERS)

$(CF8385_TARGET_CONF):
	(grep -q cfio $(TARGET_DIR)/etc/modprobe.conf || \
	 echo -e 'alias mwlan0 cfio\ninstall cfio /sbin/modprobe --ignore-install cfio && /sbin/modprobe mcf25\n' >> $(TARGET_DIR)/etc/modprobe.conf)
	(grep -q mwlan0 $(TARGET_DIR)/etc/network/interfaces || \
	 echo -e '\nauto mwlan0\niface mwlan0 inet dhcp\n	pre-up /sbin/iwconfig $$IFACE essid any txpower 100mW\n' >> $(TARGET_DIR)/etc/network/interfaces)
	touch $@

wifistix: $(CF8385_TARGET_MODULE) $(CF8385_TARGET_CONF)

wifistix-clean:
	@echo Nothing to do to clean wifistix -- must be cleaned manually

wifistix-dirclean:
	@echo Nothing to do to clean wifistix -- must be cleaned manually

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_CF8385)),y)
TARGETS+=wifistix
endif
