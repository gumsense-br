#############################################################
#
# libosip2
#
#############################################################

LIBOSIP2_NAME=libosip2
LIBOSIP2_VERSION=2.2.0


# Don't alter below this line unless you (think) you know
# what you are doing! Danger, Danger!

LIBOSIP2_SITE=ftp://ftp.gnu.org/gnu/osip
LIBOSIP2_SOURCE=$(LIBOSIP2_NAME)-$(LIBOSIP2_VERSION).tar.gz
LIBOSIP2_DIR=$(BUILD_DIR)/${shell basename $(LIBOSIP2_SOURCE) .tar.gz}


$(DL_DIR)/$(LIBOSIP2_SOURCE):
	$(WGET) -P $(DL_DIR) $(LIBOSIP2_SITE)/$(LIBOSIP2_SOURCE)

$(LIBOSIP2_DIR)/.unpacked:	$(DL_DIR)/$(LIBOSIP2_SOURCE)
	gzip -d -c $(DL_DIR)/$(LIBOSIP2_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	touch $(LIBOSIP2_DIR)/.unpacked

$(LIBOSIP2_DIR)/.configured:	$(LIBOSIP2_DIR)/.unpacked
	(cd $(LIBOSIP2_DIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) \
		CFLAGS="$(TARGET_CFLAGS)" \
		./configure \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=$(STAGING_DIR)/usr \
		--exec-prefix=$(STAGING_DIR)/usr \
		--bindir=$(STAGING_DIR)/usr/bin \
		--sbindir=$(STAGING_DIR)/usr/sbin \
		--libexecdir=$(STAGING_DIR)/usr/lib \
		--sysconfdir=$(STAGING_DIR)/etc \
		--datadir=$(STAGING_DIR)/usr/share \
		--localstatedir=$(STAGING_DIR)/var \
		--mandir=$(STAGING_DIR)/usr/man \
		--infodir=$(STAGING_DIR)/usr/info \
		--disable-shared \
		$(DISABLE_NLS) \
		);
	touch  $(LIBOSIP2_DIR)/.configured

$(LIBOSIP2_DIR)/.maked:	$(LIBOSIP2_DIR)/.configured
	$(MAKE) -C $(LIBOSIP2_DIR)
	touch $(LIBOSIP2_DIR)/.maked


$(LIBOSIP2_DIR)/.installed:	$(LIBOSIP2_DIR)/.maked
	$(MAKE) PREFIX=$(STAGING_DIR) -C $(LIBOSIP2_DIR) install
	touch $(LIBOSIP2_DIR)/.installed

$(LIBOSIP2_NAME):	uclibc zlib $(LIBOSIP2_DIR)/.installed

$(LIBOSIP2_NAME)-source: $(DL_DIR)/$(LIBOSIP2_SOURCE)

$(LIBOSIP2_NAME)-clean:
	@if [ -d $(LIBOSIP2_DIR)/Makefile ] ; then \
		$(MAKE) -C $(LIBOSIP2_DIR) clean ; \
	fi;

$(LIBOSIP2_NAME)-dirclean:
	rm -rf $(LIBOSIP2_DIR) 

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_LIBOSIP2)),y)
TARGETS+=libosip2
endif
