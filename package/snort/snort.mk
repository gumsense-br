#############################################################
#
# snort
#
#############################################################
SNORT_VERSION=2.2.0
SNORT_SOURCE_URL=http://www.snort.org/dl
SNORT_SOURCE=snort-$(SNORT_VERSION).tar.gz
SNORT_BUILD_DIR=$(BUILD_DIR)/snort-$(SNORT_VERSION)

$(DL_DIR)/$(SNORT_SOURCE):
	 $(WGET) -P $(DL_DIR) $(SNORT_SOURCE_URL)/$(SNORT_SOURCE) 

$(SNORT_BUILD_DIR)/.unpacked: $(DL_DIR)/$(SNORT_SOURCE)
	zcat $(DL_DIR)/$(SNORT_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
#	toolchain/patch-kernel.sh $(SNORT_BUILD_DIR) package/snort/snort snort-\*.patch
	touch $(SNORT_BUILD_DIR)/.unpacked

snort-unpacked: $(SNORT_BUILD_DIR)/.unpacked

$(SNORT_BUILD_DIR)/.configured: $(SNORT_BUILD_DIR)/.unpacked
	(cd $(SNORT_BUILD_DIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) \
		CFLAGS="$(TARGET_CFLAGS)" \
		./configure \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=/usr \
		--exec-prefix=/usr \
		--bindir=/usr/bin \
		--sbindir=/usr/sbin \
		--libexecdir=/usr/lib \
		--sysconfdir=/etc \
		--datadir=/usr/share/misc \
		--localstatedir=/var \
		--mandir=/usr/man \
		--infodir=/usr/info \
		--with-libpcap="$(STAGING_DIR)" \
		--with-libpcre="$(STAGING_DIR)" \
        );
	touch  $(SNORT_BUILD_DIR)/.configured

snort-configured: $(SNORT_BUILD_DIR)/.configured $(LIBPCAP_BUILD_DIR)/libpcap.a $(LIBPCRE_BUILD_DIR)/libpcre.la

$(SNORT_BUILD_DIR)/src/snort: $(SNORT_BUILD_DIR)/.configured
	$(MAKE) -C $(SNORT_BUILD_DIR)

$(TARGET_DIR)/etc/snort: $(SNORT_BUILD_DIR)/rules
	install -m 0755 -d $< $@
	install -m 0644 $</*.rules $@

$(TARGET_DIR)/usr/bin/snort: $(SNORT_BUILD_DIR)/src/snort
	install -m 0755 -D $< $@
	$(STRIP) $(TARGET_DIR)/usr/sbin/snort

snort: libpcap libpcre $(TARGET_DIR)/usr/bin/snort $(TARGET_DIR)/etc/snort

snort-source: $(DL_DIR)/$(SNORT_SOURCE)

snort-clean:
	$(MAKE) DESTDIR=$(TARGET_DIR) CC=$(TARGET_CC) -C $(SNORT_BUILD_DIR) uninstall
	-$(MAKE) -C $(SNORT_BUILD_DIR) clean

snort-dirclean:
	rm -rf $(SNORT_BUILD_DIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_SNORT)),y)
TARGETS+=snort
endif
