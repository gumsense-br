#############################################################
#
# btscanner
#
#############################################################
BTSCANNER_SOURCE:=btscanner-1.0.tar.gz
BTSCANNER_SITE:=http://files.gumstix.com
BTSCANNER_DIR:=$(BUILD_DIR)/btscanner-1.0
BTSCANNER_CAT:=gzcat
BTSCANNER_BINARY:=btscanner
BTSCANNER_TARGET_BINARY:=usr/bin/btscanner

$(DL_DIR)/$(BTSCANNER_SOURCE):
	 $(WGET) -P $(DL_DIR) $(BTSCANNER_SITE)/$(BTSCANNER_SOURCE)

btscanner-source: $(DL_DIR)/$(BTSCANNER_SOURCE)

$(BTSCANNER_DIR)/configure: $(DL_DIR)/$(BTSCANNER_SOURCE)
	$(BTSCANNER_CAT) $(DL_DIR)/$(BTSCANNER_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	toolchain/patch-kernel.sh $(BTSCANNER_DIR) package/btscanner btscanner-*.patch
	touch $(BTSCANNER_DIR)/configure

$(BTSCANNER_DIR)/Makefile: $(BTSCANNER_DIR)/configure
	(cd $(BTSCANNER_DIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) \
		CFLAGS="$(TARGET_CFLAGS)" \
		./configure \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=/ \
		--exec-prefix=/usr \
		--bindir=/usr/bin \
		--sbindir=/usr/sbin \
		--libexecdir=/usr/lib \
		--sysconfdir=/etc \
		--datadir=/usr/share/misc \
		--localstatedir=/var \
		--mandir=/usr/man \
		--infodir=/usr/info \
	);

$(BTSCANNER_DIR)/$(BTSCANNER_BINARY): $(BTSCANNER_DIR)/Makefile
	$(MAKE) -C $(BTSCANNER_DIR)

$(TARGET_DIR)/$(BTSCANNER_TARGET_BINARY): $(BTSCANNER_DIR)/$(BTSCANNER_BINARY)
	cp $(BTSCANNER_DIR)/$(BTSCANNER_BINARY) $(TARGET_DIR)/$(BTSCANNER_TARGET_BINARY)
	chmod 0755 $(TARGET_DIR)/$(BTSCANNER_TARGET_BINARY)
	$(STRIP) $@
	cp $(BTSCANNER_DIR)/oui.txt $(TARGET_DIR)/etc/oui.txt

btscanner: uclibc ncurses gdbm bluez-libs $(TARGET_DIR)/$(BTSCANNER_TARGET_BINARY)

btscanner-clean:
	rm $(TARGET_DIR)/$(BTSCANNER_TARGET_BINARY)
	-$(MAKE) -C $(BTSCANNER_DIR) clean

btscanner-dirclean:
	rm -rf $(BTSCANNER_DIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_BTSCANNER)),y)
TARGETS+=btscanner
endif
