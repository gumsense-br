#############################################################
#
# cvoicecontrol
#
#############################################################

CVC_NAME=cvoicecontrol
CVC_VERSION=0.9alpha


# Don't alter below this line unless you (think) you know
# what you are doing! Danger, Danger!

CVC_SITE=http://www.kiecza.net/daniel/linux/
CVC_SOURCE=$(CVC_NAME)-$(CVC_VERSION).tar.gz
CVC_DIR=$(BUILD_DIR)/${shell basename $(CVC_SOURCE) .tar.gz}

$(DL_DIR)/$(CVC_SOURCE):
	$(WGET) -P $(DL_DIR) $(CVC_SITE)/$(CVC_SOURCE)

$(CVC_DIR)/.unpacked:	$(DL_DIR)/$(CVC_SOURCE)
	gzip -d -c $(DL_DIR)/$(CVC_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	toolchain/patch-kernel.sh $(CVC_DIR) package/cvoicecontrol cvoicecontrol*.patch
ifeq ($(strip $(BR2_PACKAGE_CVC_LINEIN)),y)
	toolchain/patch-kernel.sh $(CVC_DIR) package/cvoicecontrol cvoice-*.patch
endif
	touch $(CVC_DIR)/.unpacked

$(CVC_DIR)/Makefile:	$(CVC_DIR)/.unpacked
	(cd $(CVC_DIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) \
		CFLAGS="$(TARGET_CFLAGS)" \
		ac_cv_c_const=yes \
		./configure \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=$(CVC_DIR)/build \
		--sysconfdir=$(STAGING_DIR)/etc \
		--disable-shared \
		$(DISABLE_NLS) \
		);

$(CVC_DIR)/cvoicecontrol/cvoicecontrol:	$(CVC_DIR)/Makefile
	$(MAKE) -C $(CVC_DIR)

$(TARGET_DIR)/usr/bin/cvoicecontrol:	$(CVC_DIR)/cvoicecontrol/cvoicecontrol
	install -D -m 0755 $(CVC_DIR)/cvoicecontrol/cvoicecontrol $(TARGET_DIR)/usr/bin/cvoicecontrol
	install -D -m 0755 $(CVC_DIR)/cvoicecontrol/microphone_config $(TARGET_DIR)/usr/bin/microphone_config
	install -D -m 0755 $(CVC_DIR)/cvoicecontrol/model_editor $(TARGET_DIR)/usr/bin/model_editor
	$(STRIP) $(TARGET_DIR)/usr/bin/cvoicecontrol
	$(STRIP) $(TARGET_DIR)/usr/bin/microphone_config
	$(STRIP) $(TARGET_DIR)/usr/bin/model_editor

$(CVC_NAME):	uclibc ncurses $(TARGET_DIR)/usr/bin/cvoicecontrol

$(CVC_NAME)-source: $(DL_DIR)/$(CVC_SOURCE)

$(CVC_NAME)-clean:
	@if [ -d $(LINPHONE_DIR)/Makefile ] ; then \
		$(MAKE) -C $(CVC_DIR) clean ; \
	fi;

$(CVC_NAME)-dirclean:
	rm -rf $(CVC_DIR) 

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_CVC)),y)
TARGETS+=cvoicecontrol
endif

