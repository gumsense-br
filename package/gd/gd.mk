#############################################################
#
# gd
#
#############################################################
GD_VERSION:=2.0.33
GD_SOURCE:=gd-$(GD_VERSION).tar.gz
GD_SITE:=http://www.boutell.com/gd/http
GD_DIR:=$(BUILD_DIR)/gd-$(GD_VERSION)
GD_LIBRARY:=libgd.la
GD_TARGET_LIBRARY:=usr/lib/libgd.so

GD_USE_PNG:=--without-png
ifeq ($(strip $(BR2_PACKAGE_LIBPNG)),y)
GD_USE_PNG:=--with-png=$(STAGING_DIR)
GD_PNG_DEP:=libpng
endif

# JPEG has link problems -- tries to link against local /usr/lib/libjpeg instead of the STAGING_DIR one
GD_USE_JPEG:=--without-jpeg
#ifeq ($(strip $(BR2_PACKAGE_JPEG)),y)
#GD_USE_JPEG:=--with-jpeg=$(STAGING_DIR)
#GD_JPEG_DEP:=jpeg
#endif

GD_USE_FREETYPE:=--without-freetype
ifeq ($(strip $(BR2_PACKAGE_FREETYPE)),y)
GD_USE_FREETYPE:=--with-freetype=$(STAGING_DIR)/usr
GD_FREETYPE_DEP:=freetype
endif

$(DL_DIR)/$(GD_SOURCE):
	$(WGET) -P $(DL_DIR) $(GD_SITE)/$(GD_SOURCE)

$(GD_DIR)/.source: $(DL_DIR)/$(GD_SOURCE)
	zcat $(DL_DIR)/$(GD_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	touch $(GD_DIR)/.source

$(GD_DIR)/.configured: $(GD_DIR)/.source
	(cd $(GD_DIR); \
		$(TARGET_CONFIGURE_OPTS) \
		CFLAGS="-I$(STAGING_DIR)/usr/include -I$(STAGING_DIR)/usr/include/freetype2/freetype -I$(STAGING_DIR)/include/libpng12 -I$(STAGING_DIR)/include/jpeg $(TARGET_CFLAGS) " \
		LDFLAGS="-L$(STAGING_DIR)/lib -L$(STAGING_DIR)/usr/lib " \
		./configure \
			--target=$(GNU_TARGET_NAME) \
			--host=$(GNU_TARGET_NAME) \
			--build=$(GNU_HOST_NAME) \
			--prefix=$(GD_DIR) \
			$(GD_USE_FREETYPE) \
			$(GD_USE_JPEG) \
			$(GD_USE_PNG) \
			--without-x \
	);
	touch $(GD_DIR)/.configured;

$(GD_DIR)/$(GD_LIBRARY): $(GD_DIR)/.configured
	$(MAKE) CC=$(TARGET_CC) -C $(GD_DIR)

$(TARGET_DIR)/$(GD_TARGET_LIBRARY): $(GD_DIR)/$(GD_LIBRARY)
	$(MAKE) CC=$(TARGET_CC) -C $(GD_DIR) install
	rm -v $(GD_DIR)/bin/gdlib-config
	rm -v $(GD_DIR)/bin/bdftogd
	mkdir -p $(STAGING_DIR)/include/libgd
	cp -v $(GD_DIR)/include/* $(STAGING_DIR)/include/libgd
	cp -v $(GD_DIR)/lib/*     $(STAGING_DIR)/lib
	cp -v $(GD_DIR)/bin/*     $(TARGET_DIR)/usr/bin
	cp -v $(GD_DIR)/lib/libgd.so* $(TARGET_DIR)/usr/lib

gd: uclibc $(GD_PNG_DEP) $(GD_JPEG_DEP) $(GD_FREETYPE_DEP) $(TARGET_DIR)/$(GD_TARGET_LIBRARY)

gd-source: $(DL_DIR)/$(GD_SOURCE)

gd-clean:
	rm $(TARGET_DIR)/usr/lib/libgd.so*

	-$(MAKE) -C $(GD_DIR) clean

gd-dirclean:
	rm -rf $(GD_DIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_GD)),y)
TARGETS+=gd
endif

