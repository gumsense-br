#############################################################
#
# netperf
#
#############################################################
#
NETPERF_SOURCE_URL=ftp://ftp.netperf.org/netperf
NETPERF_VER=2.4.2
NETPERF_SOURCE=netperf-$(NETPERF_VER).tar.gz
NETPERF_BUILD_DIR=$(BUILD_DIR)/netperf-$(NETPERF_VER)

$(DL_DIR)/$(NETPERF_SOURCE):
	$(WGET) -P $(DL_DIR) $(NETPERF_SOURCE_URL)/$(NETPERF_SOURCE)

$(NETPERF_BUILD_DIR)/.unpacked: $(DL_DIR)/$(NETPERF_SOURCE)
	cd $(BUILD_DIR)
	zcat $(DL_DIR)/$(NETPERF_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	touch $(NETPERF_BUILD_DIR)/.unpacked

$(NETPERF_BUILD_DIR)/.configured: $(NETPERF_BUILD_DIR)/.unpacked
	(cd $(NETPERF_BUILD_DIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) \
		CFLAGS="$(TARGET_CFLAGS)" \
		ac_cv_func_setpgrp_void=set \
		./configure \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=/usr \
		--exec-prefix=/usr \
		--enable-unixdomain \
		--enable-cpuutil=procstat \
		--enable-histogram \
	);
	touch  $(NETPERF_BUILD_DIR)/.configured

$(NETPERF_BUILD_DIR)/src/netperf: $(NETPERF_BUILD_DIR)/.configured
	$(MAKE) CC=$(TARGET_CC) -C $(NETPERF_BUILD_DIR)

$(TARGET_DIR)/usr/bin/netperf: $(NETPERF_BUILD_DIR)/src/netperf
	mkdir -p $(TARGET_DIR)/usr/bin
	install -m 0755 $(NETPERF_BUILD_DIR)/src/netserver $(TARGET_DIR)/usr/bin/
	$(STRIP) $(TARGET_DIR)/usr/bin/netserver
	install -m 0755 $(NETPERF_BUILD_DIR)/src/netperf $(TARGET_DIR)/usr/bin/
	$(STRIP) $<

netperf: $(TARGET_DIR)/usr/bin/netperf

netperf-source: $(DL_DIR)/$(NETPERF_SOURCE)

netperf-clean:
	rm -f $(NETPERF_BUILD_DIR)/*.o $(NETPERF_BUILD_DIR)/netperf

netperf-dirclean:
	rm -rf $(NETPERF_BUILD_DIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_NETPERF)),y)
TARGETS+=netperf
endif
