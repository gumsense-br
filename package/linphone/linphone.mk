#############################################################
#
# linphone
#
#############################################################

LINPHONE_NAME=linphone
LINPHONE_VERSION=1.1.0

LINPHONEextraCFLAGS=--no-builtin-exp --no-builtin-cos --no-builtin-sin --no-builtin-log

# Don't alter below this line unless you (think) you know
# what you are doing! Danger, Danger!

LINPHONE_SITE=http://simon.morlat.free.fr/download/1.1.x/source
LINPHONE_SOURCE=$(LINPHONE_NAME)-$(LINPHONE_VERSION).tar.gz
LINPHONE_DIR=$(BUILD_DIR)/${shell basename $(LINPHONE_SOURCE) .tar.gz}


$(DL_DIR)/$(LINPHONE_SOURCE):
	$(WGET) -P $(DL_DIR) $(LINPHONE_SITE)/$(LINPHONE_SOURCE)

$(LINPHONE_DIR)/.unpacked:	$(DL_DIR)/$(LINPHONE_SOURCE)
	gzip -d -c $(DL_DIR)/$(LINPHONE_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	toolchain/patch-kernel.sh $(LINPHONE_DIR) package/linphone linphone*.patch
	touch $(LINPHONE_DIR)/.unpacked

$(LINPHONE_DIR)/Makefile:	$(LINPHONE_DIR)/.unpacked
	(cd $(LINPHONE_DIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) \
		CFLAGS="$(TARGET_CFLAGS) $(LINPHONEextraCFLAGS)" \
		ac_cv_c_const=yes \
		./configure \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=/usr \
		--exec-prefix=/usr \
		--bindir=/usr/bin \
		--sbindir=/usr/sbin \
		--libexecdir=/usr/lib \
		--sysconfdir=/etc \
		--datadir=/usr/share \
		--localstatedir=/var \
		--mandir=/usr/man \
		--infodir=/usr/info \
		--with-osip=$(STAGING_DIR)/usr \
		--with-speex=$(STAGING_DIR)/usr \
		--without-ilbc \
		--enable-gnome_ui=no \
		--disable-manual \
		--disable-glib \
		--disable-shared \
		--with-real-prefix=/usr \
		$(DISABLE_NLS) \
		);


$(LINPHONE_DIR)/console/linphonec: $(LINPHONE_DIR)/Makefile
	$(MAKE) -C $(LINPHONE_DIR)


$(TARGET_DIR)/usr/bin/linphonec: $(LINPHONE_DIR)/console/linphonec
	install -D -m 0755 $< $@
	$(STRIP) $@

$(LINPHONE_NAME): libosip2 speex uclibc zlib $(TARGET_DIR)/usr/bin/linphonec

$(LINPHONE_NAME)-source: $(DL_DIR)/$(LINPHONE_SOURCE)

$(LINPHONE_NAME)-clean:
	@if [ -d $(LINPHONE_DIR)/Makefile ] ; then \
		$(MAKE) -C $(LINPHONE_DIR) clean ; \
	fi;

$(LINPHONE_NAME)-dirclean:
	rm -rf $(LINPHONE_DIR) 

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_LINPHONE)),y)
TARGETS+=linphone
endif
