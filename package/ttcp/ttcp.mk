#############################################################
#
# ttcp
#
#############################################################
#
TTCP_SOURCE_URL=http://ftp.sunet.se/pub/network/monitoring/ttcp
TTCP_SOURCE=ttcp.c
TTCP_BUILD_DIR=$(BUILD_DIR)/ttcp

$(DL_DIR)/$(TTCP_SOURCE):
	 $(WGET) -P $(DL_DIR) $(TTCP_SOURCE_URL)/$(TTCP_SOURCE) 

$(TTCP_BUILD_DIR)/ttcp.c: $(DL_DIR)/$(TTCP_SOURCE)
	-mkdir -p $(TTCP_BUILD_DIR)
	cp -Rf $(DL_DIR)/$(TTCP_SOURCE) $(TTCP_BUILD_DIR)

$(TTCP_BUILD_DIR)/ttcp: $(TTCP_BUILD_DIR)/ttcp.c
	$(TARGET_CC) $(TARGET_CFLAGS) -o $(TTCP_BUILD_DIR)/ttcp $(TTCP_BUILD_DIR)/$(TTCP_SOURCE) 

$(TARGET_DIR)/usr/bin/ttcp: $(TTCP_BUILD_DIR)/ttcp
	cp -Rf $(TTCP_BUILD_DIR)/ttcp $(TARGET_DIR)/usr/bin/
	$(STRIP) $<

ttcp: $(TARGET_DIR)/usr/bin/ttcp 

ttcp-source: $(DL_DIR)/$(TTCP_SOURCE)

ttcp-clean:
	rm -f $(TTCP_BUILD_DIR)/*.o $(TTCP_BUILD_DIR)/ttcp	

ttcp-dirclean:
	rm -rf $(TTCP_BUILD_DIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_TTCP)),y)
TARGETS+=ttcp
endif
