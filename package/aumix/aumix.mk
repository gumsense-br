#############################################################
#
# aumix
#
#############################################################
AUMIX_SOURCE:=aumix-2.8.tar.bz2
AUMIX_SITE:=http://www.jpj.net/~trevor/aumix
AUMIX_DIR:=$(BUILD_DIR)/aumix-2.8
AUMIX_CAT:=bzcat
AUMIX_BINARY:=src/aumix
AUMIX_TARGET_BINARY:=usr/bin/aumix

$(DL_DIR)/$(AUMIX_SOURCE):
	 $(WGET) -P $(DL_DIR) $(AUMIX_SITE)/$(AUMIX_SOURCE)

aumix-source: $(DL_DIR)/$(AUMIX_SOURCE)

$(AUMIX_DIR)/.unpacked: $(DL_DIR)/$(AUMIX_SOURCE)
	$(AUMIX_CAT) $(DL_DIR)/$(AUMIX_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	toolchain/patch-kernel.sh $(AUMIX_DIR) package/aumix aumix*.patch
	touch  $(AUMIX_DIR)/.unpacked

$(AUMIX_DIR)/.configured: $(AUMIX_DIR)/.unpacked
	(cd $(AUMIX_DIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) \
		CFLAGS="$(TARGET_CFLAGS)" \
		./configure \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=/usr \
		--exec-prefix=/usr \
		--bindir=/usr/bin \
		--sbindir=/usr/sbin \
		--libexecdir=/usr/lib \
		--sysconfdir=/etc \
		--datadir=/usr/share/misc \
		--localstatedir=/var \
		--mandir=/usr/man \
		--infodir=/usr/info \
		--includedir=$(STAGING_DIR)/include \
		--without-ncurses \
		--without-gpm \
		--without-sysmouse \
		--without-alsa \
		--without-gtk \
		--without-gtk1 \
	);
	touch  $(AUMIX_DIR)/.configured

$(AUMIX_DIR)/$(AUMIX_BINARY): $(AUMIX_DIR)/.configured
	$(MAKE) LDADD="" -C $(AUMIX_DIR)

$(TARGET_DIR)/$(AUMIX_TARGET_BINARY): $(AUMIX_DIR)/$(AUMIX_BINARY)
	cp $(AUMIX_DIR)/$(AUMIX_BINARY) $(TARGET_DIR)/$(AUMIX_TARGET_BINARY)
	-($(STRIP) $(TARGET_DIR)/$(AUMIX_TARGET_BINARY) > /dev/null 2>&1)

aumix: zlib uclibc $(TARGET_DIR)/$(AUMIX_TARGET_BINARY)

aumix-clean:
	$(MAKE) DESTDIR=$(TARGET_DIR) CC=$(TARGET_CC) -C $(AUMIX_DIR) uninstall
	-$(MAKE) -C $(AUMIX_DIR) clean

aumix-dirclean:
	rm -rf $(AUMIX_DIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_AUMIX)),y)
TARGETS+=aumix
endif
