#############################################################
#
# ethtool
#
#############################################################
ETHTOOL_SOURCE:=ethtool-3.tar.gz
ETHTOOL_SITE:=http://easynews.dl.sourceforge.net/sourceforge/gkernel
ETHTOOL_CAT:=zcat
ETHTOOL_DIR:=$(BUILD_DIR)/ethtool-3
ETHTOOL_BINARY:=ethtool
ETHTOOL_TARGET_BINARY:=usr/bin/ethtool

$(DL_DIR)/$(ETHTOOL_SOURCE):
	 $(WGET) -P $(DL_DIR) $(ETHTOOL_SITE)/$(ETHTOOL_SOURCE)

ethtool-source: $(DL_DIR)/$(ETHTOOL_SOURCE)

$(ETHTOOL_DIR)/.unpacked: $(DL_DIR)/$(ETHTOOL_SOURCE)
	$(ETHTOOL_CAT) $(DL_DIR)/$(ETHTOOL_SOURCE) | tar -C $(BUILD_DIR) -xf -
	touch $(ETHTOOL_DIR)/.unpacked

$(ETHTOOL_DIR)/.configured: $(ETHTOOL_DIR)/.unpacked
	(cd $(ETHTOOL_DIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) \
		CFLAGS="$(TARGET_CFLAGS)" \
		./configure \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=/usr \
		--exec-prefix=/usr \
		$(DISABLE_NLS) \
	);
	touch  $(ETHTOOL_DIR)/.configured

$(ETHTOOL_DIR)/$(ETHTOOL_BINARY): $(ETHTOOL_DIR)/.configured
	$(MAKE) CC=$(TARGET_CC) -C $(ETHTOOL_DIR)

$(TARGET_DIR)/$(ETHTOOL_TARGET_BINARY): $(ETHTOOL_DIR)/$(ETHTOOL_BINARY)
	cp -a $(ETHTOOL_DIR)/$(ETHTOOL_BINARY) $(TARGET_DIR)/$(ETHTOOL_TARGET_BINARY)

ethtool: uclibc $(TARGET_DIR)/$(ETHTOOL_TARGET_BINARY)

ethtool-clean:
	$(MAKE) DESTDIR=$(TARGET_DIR) CC=$(TARGET_CC) -C $(ETHTOOL_DIR) uninstall
	-$(MAKE) -C $(ETHTOOL_DIR) clean

ethtool-dirclean:
	rm -rf $(ETHTOOL_DIR)


#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_ETHTOOL)),y)
TARGETS+=ethtool
endif
