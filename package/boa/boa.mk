#############################################################
#
# boa
#
#############################################################

BOA_VERSION=0.94.14rc20

# Don't alter below this line unless you (think) you know
# what you are doing! Danger, Danger!

BOA_SOURCE=boa-$(BOA_VERSION).tar.gz
BOA_SITE=http://www.boa.org/
BOA_DIR=$(BUILD_DIR)/${shell basename $(BOA_SOURCE) .tar.gz}
BOA_WORKDIR=$(BUILD_DIR)/boa_workdir

$(DL_DIR)/$(BOA_SOURCE):
	$(WGET) -P $(DL_DIR) $(BOA_SITE)/$(BOA_SOURCE)

$(BOA_DIR)/.unpacked:	$(DL_DIR)/$(BOA_SOURCE)
	gzip -d -c $(DL_DIR)/$(BOA_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	touch $(BOA_DIR)/.unpacked

$(BOA_WORKDIR)/Makefile: $(BOA_DIR)/.unpacked
	rm -f $(BOA_WORKDIR)/Makefile
	mkdir -p $(BOA_WORKDIR)
	#CONFIG_SITE=package/boa/boa-config.site-$(ARCH)
	(cd $(BOA_WORKDIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) \
		CFLAGS="$(TARGET_CFLAGS)" \
		CPPFLAGS="$(SED_CFLAGS)" \
		$(BOA_DIR)/configure \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=/usr \
		--exec-prefix=/usr \
		--bindir=/usr/bin \
		--sbindir=/usr/sbin \
		--libexecdir=/usr/lib \
		--sysconfdir=/etc \
		--datadir=/usr/share \
		--localstatedir=/var \
		--mandir=/usr/man \
		--infodir=/usr/info \
		$(DISABLE_NLS) \
	);
	touch $(BOA_WORKDIR)/Makefile

$(BOA_WORKDIR)/src/boa $(BOA_WORKDIR)/src/boa_indexer:	$(BOA_WORKDIR)/Makefile
	rm -f $@
	$(MAKE) -C $(BOA_WORKDIR)

$(TARGET_DIR)/usr/sbin/boa: $(BOA_WORKDIR)/src/boa $(BOA_WORKDIR)/src/boa_indexer
	mkdir -p $(TARGET_DIR)/usr/sbin
	cp -f $(BOA_WORKDIR)/src/boa $(TARGET_DIR)/usr/sbin/boa
	mkdir -p $(TARGET_DIR)/usr/lib/boa
	cp -f $(BOA_WORKDIR)/src/boa_indexer $(TARGET_DIR)/usr/lib/boa/boa_indexer
	mkdir -p $(TARGET_DIR)/etc/boa
	cp -f $(BOA_DIR)/examples/boa.conf $(TARGET_DIR)/etc/boa
	$(SED) 's/^User .*$$/User www-data/' $(TARGET_DIR)/etc/boa/boa.conf
	$(SED) 's/^Group .*$$/Group www-data/' $(TARGET_DIR)/etc/boa/boa.conf
	$(SED) 's~^AccessLog .*$$~AccessLog /var/log/boa_access_log~' $(TARGET_DIR)/etc/boa/boa.conf
	$(SED) 's~^ErrorLog .*$$~ErrorLog /var/log/boa_error_log~' $(TARGET_DIR)/etc/boa/boa.conf
	$(SED) 's~/usr/lib/cgi-bin~/var/www/cgi-bin~' $(TARGET_DIR)/etc/boa/boa.conf
	cp -f package/boa/mime.types $(TARGET_DIR)/etc/mime.types
	cp -f package/boa/boa-S50httpd $(TARGET_DIR)/etc/init.d/S50httpd
	$(STRIP) $(TARGET_DIR)/usr/sbin/boa $(TARGET_DIR)/usr/lib/boa/boa_indexer
	mkdir -p $(TARGET_DIR)/var
	cp -dpPRf package/boa/var/www $(TARGET_DIR)/var
	rm -rf $(TARGET_DIR)/var/www/.svn $(TARGET_DIR)/var/www/cgi-bin/.svn

boa:	uclibc $(TARGET_DIR)/usr/sbin/boa

boa-source: $(DL_DIR)/$(BOA_SOURCE)

boa-clean:
	@if [ -d $(BOA_WORKDIR)/Makefile ] ; then \
		$(MAKE) -C $(BOA_WORKDIR) clean ; \
	fi;

boa-dirclean:
	rm -rf $(BOA_DIR) $(BOA_WORKDIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_BOA)),y)
TARGETS+=boa
endif
