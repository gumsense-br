#############################################################
#
# libao, shamelessly modified from libid3tag
#
#############################################################

LIBAO_VERSION=0.8.6

# Don't alter below this line unless you (think) you know
# what you are doing! Danger, Danger!

LIBAO_SOURCE=libao-$(LIBAO_VERSION).tar.gz
LIBAO_SITE=http://downloads.xiph.org/releases/ao
LIBAO_DIR=$(BUILD_DIR)/${shell basename $(LIBAO_SOURCE) .tar.gz}
LIBAO_WORKDIR=$(BUILD_DIR)/libao-$(LIBAO_VERSION)

$(DL_DIR)/$(LIBAO_SOURCE):
	$(WGET) -P $(DL_DIR) $(LIBAO_SITE)/$(LIBAO_SOURCE)

$(LIBAO_DIR)/.unpacked:	$(DL_DIR)/$(LIBAO_SOURCE)
	gzip -d -c $(DL_DIR)/$(LIBAO_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	touch $(LIBAO_DIR)/.unpacked

$(LIBAO_DIR)/.configured: $(LIBAO_DIR)/.unpacked
	(cd $(LIBAO_DIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) \
		CFLAGS="$(TARGET_CFLAGS)" \
		./configure \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=/usr \
		--sysconfdir=/etc \
		--disable-esd \
		--disable-esdtest \
		--disable-alsa \
		--disable-alsa09 \
		--disable-arts \
		--disable-nas \
		--disable-polyp \
		--enable-oss \
		--disable-static \
		--enable-shared \
		$(DISABLE_NLS) \
	);
	touch  $(LIBAO_DIR)/.configured

$(LIBAO_WORKDIR)/.libs:	$(LIBAO_DIR)/.configured
	$(MAKE) CC=$(TARGET_CC) -C $(LIBAO_WORKDIR)

$(STAGING_DIR)/lib/libao.so.2.1.3: 	$(LIBAO_WORKDIR)/.libs
	$(MAKE) prefix=$(STAGING_DIR) -C $(LIBAO_WORKDIR) install

$(TARGET_DIR)/lib/libao.so.2.1.3: $(STAGING_DIR)/lib/libao.so.2.1.3
	cp -dpf $(STAGING_DIR)/lib/libao.so* $(TARGET_DIR)/lib;
	-$(STRIP) $(TARGET_DIR)/lib/libao.so*
	touch -c $(TARGET_DIR)/lib/libao.so.2.1.3
	mkdir -p $(TARGET_DIR)/usr/lib/ao/plugins-2
	cp -pdf $(STAGING_DIR)/lib/ao/plugins-2/liboss.so $(TARGET_DIR)/usr/lib/ao/plugins-2/liboss.so

libao:	uclibc $(TARGET_DIR)/lib/libao.so.2.1.3

libao-source: $(DL_DIR)/$(LIBAO_SOURCE)

libao-clean:
	@if [ -d $(LIBAO_WORKDIR)/Makefile ] ; then \
		$(MAKE) -C $(LIBAO_WORKDIR) clean ; \
	fi;

libao-dirclean:
	rm -rf $(LIBAO_DIR) $(LIBAO_WORKDIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_LIBAO)),y)
TARGETS+=libao
endif
