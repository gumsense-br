#############################################################
#
# nmap
#
#############################################################
#
NMAP_SOURCE_URL=http://freshmeat.net/redir/nmap/7202/url_tgz
NMAP_SOURCE=nmap-4.11.tgz
NMAP_BUILD_DIR=$(BUILD_DIR)/nmap-4.11

$(DL_DIR)/$(NMAP_SOURCE):
	$(WGET) -P $(DL_DIR) $(NMAP_SOURCE_URL)/$(NMAP_SOURCE)

$(NMAP_BUILD_DIR)/configure: $(DL_DIR)/$(NMAP_SOURCE)
	zcat $(DL_DIR)/$(NMAP_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	toolchain/patch-kernel.sh $(NMAP_BUILD_DIR) package/nmap nmap*.patch

$(NMAP_BUILD_DIR)/Makefile: $(NMAP_BUILD_DIR)/configure
	(cd $(NMAP_BUILD_DIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) CFLAGS="$(TARGET_CFLAGS)" CXXPROG="AVAILABLE" \
		./configure \
		--without-nmapfe \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=/usr \
		--exec-prefix=/usr \
		--bindir=/usr/bin \
		--sbindir=/usr/sbin \
		--libexecdir=/usr/lib \
		--sysconfdir=/etc \
		--datadir=/usr/share \
		--localstatedir=/var \
		--mandir=/usr/man \
		--with-openssl=$(STAGING_DIR) \
		--with-libpcap=$(STAGING_DIR) \
		--with-libpcre=$(STAGING_DIR) \
		--infodir=/usr/info );

$(NMAP_BUILD_DIR)/nmap: $(NMAP_BUILD_DIR)/Makefile
	$(MAKE) $(JLEVEL) -C $(NMAP_BUILD_DIR)

$(TARGET_DIR)/usr/sbin/nmap: $(NMAP_BUILD_DIR)/nmap
	install -m 0755 $(NMAP_BUILD_DIR)/nmap $(TARGET_DIR)/usr/sbin
	$(STRIP) $(TARGET_DIR)/usr/sbin/nmap

.PHONY: nmap-source nmap

nmap: openssl libpcap libpcre $(TARGET_DIR)/usr/sbin/nmap

nmap-source: $(DL_DIR)/$(NMAP_SOURCE)

nmap-clean:
	-$(MAKE) -C $(NMAP_BUILD_DIR) clean

nmap-dirclean:
	rm -rf $(NMAP_BUILD_DIR)
#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_NMAP)),y)
TARGETS+=nmap
endif
