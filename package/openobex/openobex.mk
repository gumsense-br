#############################################################
#
# openobex
#
#############################################################
OPENOBEX_SITE:=http://umn.dl.sourceforge.net/sourceforge/openobex
OPENOBEX_CAT:=zcat
OPENOBEX_SOURCE:=openobex-1.2.tar.gz
OPENOBEX_DIR:=$(BUILD_DIR)/openobex-1.2
OPENOBEX_LIB:=lib/.libs/libopenobex.so
OPENOBEX_TARGET_LIB:=usr/lib/libopenobex.so
OBEX_SERVER_SITE:=http://www.frasunek.com/sources/unix
OBEX_SERVER_SOURCE:=obexserver.c
OBEX_SERVER_DIR:=$(BUILD_DIR)/obexserver
OBEX_SERVER_TARGET_BIN:=usr/sbin/obexserver
OPD_SITE:=http://oss.bdit.de/download
OPD_SOURCE:=opd-v0.2-2003-03-18.tgz
OPD_DIR:=$(BUILD_DIR)/opd
OPD_TARGET_BIN:=usr/sbin/opd

$(DL_DIR)/$(OPENOBEX_SOURCE):
	$(WGET) -P $(DL_DIR) $(OPENOBEX_SITE)/$(OPENOBEX_SOURCE)

$(DL_DIR)/$(OBEX_SERVER_SOURCE):
	$(WGET) -P $(DL_DIR) $(OBEX_SERVER_SITE)/$(OBEX_SERVER_SOURCE)

$(DL_DIR)/$(OPD_SOURCE):
	$(WGET) -P $(DL_DIR) $(OPD_SITE)/$(OPD_SOURCE)

openobex-source: $(DL_DIR)/$(OPENOBEX_SOURCE) $(DL_DIR)/$(OBEX_SERVER_SOURCE) $(DL_DIR)/$(OPD_SOURCE)

$(OPENOBEX_DIR)/configure: $(DL_DIR)/$(OPENOBEX_SOURCE)
	$(OPENOBEX_CAT) $(DL_DIR)/$(OPENOBEX_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	toolchain/patch-kernel.sh $(OPENOBEX_DIR) package/openobex openobex-*.patch
	touch $@

$(OBEX_SERVER_DIR)/$(OBEX_SERVER_SOURCE): $(DL_DIR)/$(OBEX_SERVER_SOURCE)
	mkdir -p $(OBEX_SERVER_DIR)
	cp $< $@
	toolchain/patch-kernel.sh $(OBEX_SERVER_DIR) package/openobex obexserver-*.patch

$(OPD_DIR)/Makefile: $(DL_DIR)/$(OPD_SOURCE)
	mkdir -p $(OPD_DIR)
	$(OPENOBEX_CAT) $(DL_DIR)/$(OPD_SOURCE) | tar -C $(OPD_DIR) $(TAR_OPTIONS) -
	toolchain/patch-kernel.sh $(OPD_DIR) package/openobex opd-*.patch
	touch $@

$(OPENOBEX_DIR)/Makefile: $(OPENOBEX_DIR)/configure
	(cd $(OPENOBEX_DIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) \
		CFLAGS="$(TARGET_CFLAGS)" \
		./configure \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=/usr \
		--exec-prefix=/usr \
		--bindir=/usr/bin \
		--sbindir=/usr/sbin \
		--libexecdir=/usr/lib \
		--sysconfdir=/etc \
		--datadir=/usr/share \
		--localstatedir=/var \
		--mandir=/usr/man \
		--infodir=/usr/info \
		--disable-static \
		--disable-irda \
		--disable-usb \
		--disable-fortify \
		--enable-bluetooth \
		--enable-apps \
		--with-bluez="$(STAGING_DIR)" \
		--with-usb="$(STAGING_DIR)" \
		$(DISABLE_NLS) \
	);

$(OPENOBEX_DIR)/$(OPENOBEX_LIB): $(OPENOBEX_DIR)/Makefile
	$(MAKE) -C $(OPENOBEX_DIR)

$(STAGING_DIR)/lib/libopenobex.so $(TARGET_DIR)/$(OPENOBEX_TARGET_LIB): $(OPENOBEX_DIR)/$(OPENOBEX_LIB)
	$(MAKE) DESTDIR=$(TARGET_DIR) -C $(OPENOBEX_DIR) install
	rm -rf $(TARGET_DIR)/share/locale $(TARGET_DIR)/usr/info \
		$(TARGET_DIR)/usr/man $(TARGET_DIR)/usr/share/doc \
		$(STAGING_DIR)/include/openobex
	mv $(TARGET_DIR)/usr/include/openobex $(STAGING_DIR)/include
	cp -Ppf $(TARGET_DIR)/usr/lib/libopenobex* $(STAGING_DIR)/lib

$(TARGET_DIR)/$(OBEX_SERVER_TARGET_BIN): $(OBEX_SERVER_DIR)/$(OBEX_SERVER_SOURCE) $(TARGET_DIR)/$(OPENOBEX_TARGET_LIB)
	mkdir -p $(TARGET_DIR)/usr/sbin
	$(TARGET_CC) $(TARGET_CFLAGS) -lopenobex -o $@ $(OBEX_SERVER_DIR)/$(OBEX_SERVER_SOURCE) $(OPENOBEX_DIR)/apps/libmisc.a
	$(STRIP) $@

$(TARGET_DIR)/$(OPD_TARGET_BIN): $(OPD_DIR)/Makefile $(TARGET_DIR)/$(OPENOBEX_TARGET_LIB)
	(cd $(OPD_DIR); \
	mkdir -p $(TARGET_DIR)/usr/sbin; \
	 $(TARGET_CC) $(TARGET_CFLAGS) -lbluetooth -lopenobex -o $@ main.c lib.c obex_handler.c \
	)

openobex: uclibc bluez-libs-staging bluez-libs $(TARGET_DIR)/$(OPENOBEX_TARGET_LIB) $(TARGET_DIR)/$(OBEX_SERVER_TARGET_BIN) $(TARGET_DIR)/$(OPD_TARGET_BIN)

openobex-clean:
	$(MAKE) DESTDIR=$(TARGET_DIR) CC=$(TARGET_CC) -C $(OPENOBEX_DIR) uninstall
	-$(MAKE) -C $(OPENOBEX_DIR) clean

openobex-dirclean:
	rm -rf $(OPENOBEX_DIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_OPENOBEX)),y)
TARGETS+=openobex
endif
