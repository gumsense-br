#############################################################
#
# bwmon
#
#############################################################
BWMON_SOURCE=bwmon-1.3.tar.gz
BWMON_SITE:=http://$(BR2_SOURCEFORGE_MIRROR).dl.sourceforge.net/sourceforge/bwmon
BWMON_DIR=$(BUILD_DIR)/bwmon-1.3


$(DL_DIR)/$(BWMON_SOURCE):
	$(WGET) -P $(DL_DIR) $(BWMON_SITE)/$(BWMON_SOURCE)

$(BWMON_DIR): $(DL_DIR)/$(BWMON_SOURCE)
	zcat $(DL_DIR)/$(BWMON_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -

$(BWMON_DIR)/bwmon: $(BWMON_DIR)
	$(MAKE) CC="$(TARGET_CC)" CFLAGS="$(TARGET_CFLAGS) -I../include -D__THREADS" -C $(BWMON_DIR)
	-$(STRIP) $(BWMON_DIR)/bwmon;

$(TARGET_DIR)/usr/bin/bwmon: $(BWMON_DIR)/bwmon
	install -D -m 0755 $< $@

bwmon: uclibc ncurses $(TARGET_DIR)/usr/bin/bwmon

bwmon-source: $(DL_DIR)/$(BWMON_SOURCE)

bwmon-clean:
	rm -f $(TARGET_DIR)/usr/bin/bwmon
	rm -f $(BWMON_DIR)/bwmon
	-$(MAKE) -C $(BWMON_DIR) clean

bwmon-dirclean:
	rm -rf $(BWMON_DIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_BWMON)),y)
TARGETS+=bwmon
endif
