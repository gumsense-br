#############################################################
#
# libid3tag
#
#############################################################

LIBID3TAG_VERSION=0.15.1b

# Don't alter below this line unless you (think) you know
# what you are doing! Danger, Danger!

LIBID3TAG_SOURCE=libid3tag-$(LIBID3TAG_VERSION).tar.gz
LIBID3TAG_SITE=ftp://ftp.mars.org/pub/mpeg/
LIBID3TAG_DIR=$(BUILD_DIR)/${shell basename $(LIBID3TAG_SOURCE) .tar.gz}
LIBID3TAG_WORKDIR=$(BUILD_DIR)/libid3tag-$(LIBID3TAG_VERSION)

$(DL_DIR)/$(LIBID3TAG_SOURCE):
	$(WGET) -P $(DL_DIR) $(LIBID3TAG_SITE)/$(LIBID3TAG_SOURCE)

$(LIBID3TAG_DIR)/.unpacked:	$(DL_DIR)/$(LIBID3TAG_SOURCE)
	gzip -d -c $(DL_DIR)/$(LIBID3TAG_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	touch $(LIBID3TAG_DIR)/.unpacked

$(LIBID3TAG_DIR)/.configured: $(LIBID3TAG_DIR)/.unpacked
	(cd $(LIBID3TAG_DIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) \
		CFLAGS="$(TARGET_CFLAGS)" \
		./configure \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=/usr \
		--sysconfdir=/etc \
		--enable-fpm=arm \
		--disable-shared \
		--enable-speed \
		$(DISABLE_NLS) \
	);
	touch  $(LIBID3TAG_DIR)/.configured

$(LIBID3TAG_WORKDIR)/.libs:	$(LIBID3TAG_DIR)/.configured
	$(MAKE) CC=$(TARGET_CC) -C $(LIBID3TAG_WORKDIR)

$(STAGING_DIR)/lib/libid3tag.a: 	$(LIBID3TAG_WORKDIR)/.libs
	$(MAKE) prefix=$(STAGING_DIR) -C $(LIBID3TAG_WORKDIR) install

libid3tag:	uclibc $(STAGING_DIR)/lib/libid3tag.a

libid3tag-source: $(DL_DIR)/$(LIBID3TAG_SOURCE)

libid3tag-clean:
	@if [ -d $(LIBID3TAG_WORKDIR)/Makefile ] ; then \
		$(MAKE) -C $(LIBID3TAG_WORKDIR) clean ; \
	fi;

libid3tag-dirclean:
	rm -rf $(LIBID3TAG_DIR) $(LIBID3TAG_WORKDIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_LIBID3TAG)),y)
TARGETS+=libid3tag
endif
