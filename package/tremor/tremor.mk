#############################################################
#
# tremor, ogg integer library
#
#############################################################


# Don't alter below this line unless you (think) you know
# what you are doing! Danger, Danger!

TREMOR_SOURCE=tremor
TREMOR_SVN=http://svn.xiph.org/trunk/Tremor
TREMOR_DIR=$(BUILD_DIR)/tremor
TREMOR_WORKDIR=$(BUILD_DIR)/tremor

$(DL_DIR)/$(TREMOR_SOURCE): 
	svn co $(TREMOR_SVN) $(DL_DIR)/$(TREMOR_SOURCE)
	touch $(DL_DIR)/$(TREMOR_SOURCE)/.checked

$(TREMOR_DIR)/.unpacked: $(DL_DIR)/$(TREMOR_SOURCE)
	cp -a $(DL_DIR)/$(TREMOR_SOURCE) $(BUILD_DIR)
	(cd $(TREMOR_DIR); ./autogen.sh );
	touch $(TREMOR_DIR)/.unpacked

$(TREMOR_DIR)/.configured: $(TREMOR_DIR)/.unpacked
	(cd $(TREMOR_DIR); rm -rf config.cache; \
		$(TARGET_CONFIGURE_OPTS) \
		CFLAGS="$(TARGET_CFLAGS)" \
		./configure \
		--target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=/usr \
		--sysconfdir=/etc \
		--disable-shared \
		$(DISABLE_NLS) \
	);
	touch  $(TREMOR_DIR)/.configured

$(TREMOR_WORKDIR)/.libs:	$(TREMOR_DIR)/.configured
	$(MAKE) CC=$(TARGET_CC) -C $(TREMOR_WORKDIR)

$(STAGING_DIR)/lib/libvorbisidec.a: 	$(TREMOR_WORKDIR)/.libs
	$(MAKE) prefix=$(STAGING_DIR) -C $(TREMOR_WORKDIR) install

tremor:	uclibc $(STAGING_DIR)/lib/libvorbisidec.a

tremor-source: $(DL_DIR)/$(TREMOR_SOURCE)

tremor-clean:
	@if [ -d $(TREMOR_WORKDIR)/Makefile ] ; then \
		$(MAKE) -C $(TREMOR_WORKDIR) clean ; \
	fi;

tremor-dirclean:
	rm -rf $(TREMOR_DIR) $(TREMOR_WORKDIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_TREMOR)),y)
TARGETS+=tremor
endif
