#############################################################
#
# ckermit
#
#############################################################

CKERMIT_SITE:=ftp://kermit.columbia.edu/kermit/archives
CKERMIT_SOURCE:=cku211.tar.gz
CKERMIT_DIR:=$(BUILD_DIR)/ckermit

$(DL_DIR)/$(CKERMIT_SOURCE):
	$(WGET) -P $(DL_DIR) $(CKERMIT_SITE)/$(CKERMIT_SOURCE)

ckermit-source: $(DL_DIR)/$(CKERMIT_SOURCE)

$(CKERMIT_DIR)/.unpacked: $(DL_DIR)/$(CKERMIT_SOURCE)
	mkdir -p $(CKERMIT_DIR)
	zcat $(DL_DIR)/$(CKERMIT_SOURCE) | tar -C $(CKERMIT_DIR) $(TAR_OPTIONS) -
	toolchain/patch-kernel.sh $(CKERMIT_DIR) package/ckermit ckermit-*.patch
	touch  $(CKERMIT_DIR)/.unpacked

$(CKERMIT_DIR)/wermit: $(CKERMIT_DIR)/.unpacked
	$(MAKE) $(JLEVEL) CC="$(HOSTCC)" CC2="$(HOSTCC)" -C $(CKERMIT_DIR) wart
	$(MAKE) $(JLEVEL) CC="$(TARGET_CC)" CC2="$(TARGET_CC)" KFLAGS="$(TARGET_CFLAGS)" -C $(CKERMIT_DIR) linuxgum
	$(STRIP) $(CKERMIT_DIR)/wermit

$(TARGET_DIR)/usr/bin/wermit: $(CKERMIT_DIR)/wermit
	cp $(CKERMIT_DIR)/wermit $(TARGET_DIR)/usr/bin

ckermit: uclibc $(TARGET_DIR)/usr/bin/wermit

ckermit-clean:
	rm -f $(TARGET_DIR)/usr/bin/wermit
	-$(MAKE) -C $(CKERMIT_DIR) clean

ckermit-dirclean:
	rm -rf $(CKERMIT_DIR)
#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_CKERMIT)),y)
TARGETS+=ckermit
endif
