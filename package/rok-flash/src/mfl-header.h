/*
 *
 * ROK-104001 flashing application
 *
 * Author: Craig Hughes <craig@gumstix.com>
 * Please send patches to Craig at the above email address
 * Latest version available from: $HeadURL$
 * This version: $Id$
 *
 * Copyright (c) 2005, Gumstix, Inc. All rights reserved.
 *
 * 	Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 	- Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * - Neither the name of Gumstix, Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * 	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef __MFL_HEADER_H__
#define __MFL_HEADER_H__

#define NAMESZ 12

#ifndef uint32_t
#define uint32_t u_int32_t
#endif

typedef	struct {
	unsigned char	ImageType;
	unsigned char	SectionVersion;
	char			CrcWord;
	uint32_t	SegmentSize;	//	4	bytes
	uint32_t	SegmentOffset;
	uint32_t	SmcAddr;
	unsigned char	SegmentName[NAMESZ];
} ImageHeader;

typedef	struct {
	unsigned char	MflSign[3];
	unsigned char	MflVers;
	unsigned char	ChipType;
	unsigned char	NoOfImages;
} MflHeader;


// sizeof() won't work below, due to alignment crapola
#define	MFLHDR 6
#define	IMAGEHDR 28

#endif
