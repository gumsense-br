ROK_BUILD_DIR=$(BUILD_DIR)/rok-flash
UARTBOOT_MFL=uartboot_P2A.mfl
FLASHBOOT_MFL=flashboot_P2A.mfl
APPLICATION_MFL=R2D_2_19003-cxc1210001_1_uen_e.mfl

$(ROK_BUILD_DIR)/rok-flash.c: package/rok-flash/rok-flash/rok-flash.c
	mkdir -p $(ROK_BUILD_DIR)
	cp package/rok-flash/rok-flash/* $(ROK_BUILD_DIR)

$(ROK_BUILD_DIR)/rok-flash: $(ROK_BUILD_DIR)/rok-flash.c $(ROK_BUILD_DIR)/rok-flash.h $(ROK_BUILD_DIR)/mfl-header.h
	(cd $(ROK_BUILD_DIR);$(TARGET_CC) $(TARGET_CFLAGS) -o rok-flash rok-flash.c)

$(TARGET_DIR)/usr/sbin/rok-flash: $(ROK_BUILD_DIR)/rok-flash
	install -D -m 0755 $< $@
	$(STRIP) $@

$(TARGET_DIR)/usr/share/rok/$(UARTBOOT_MFL): package/rok-flash/$(UARTBOOT_MFL)
	install -D -m 0644 $< $@

$(TARGET_DIR)/usr/share/rok/$(FLASHBOOT_MFL): package/rok-flash/$(FLASHBOOT_MFL)
	install -D -m 0644 $< $@

$(TARGET_DIR)/usr/share/rok/$(APPLICATION_MFL): package/rok-flash/$(APPLICATION_MFL)
	install -D -m 0644 $< $@

rok-flash: $(TARGET_DIR)/usr/sbin/rok-flash $(TARGET_DIR)/usr/share/rok/$(UARTBOOT_MFL) $(TARGET_DIR)/usr/share/rok/$(FLASHBOOT_MFL) $(TARGET_DIR)/usr/share/rok/$(APPLICATION_MFL)
#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_ROK-FLASH)),y)
TARGETS+=rok-flash
endif
