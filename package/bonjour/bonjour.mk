#############################################################
#
# mDNSResponder for Bonjour announcements
#
#############################################################
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Library General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
# USA

BONJOUR_SOURCE:=mDNSResponder-107.5.tar.gz
BONJOUR_SITE:=http://files.gumstix.com
BONJOUR_DIR:=$(BUILD_DIR)/mDNSResponder-107.5

$(DL_DIR)/$(BONJOUR_SOURCE):
	$(WGET) -P $(DL_DIR) $(BONJOUR_SITE)/$(BONJOUR_SOURCE)

bonjour-source: $(DL_DIR)/$(BONJOUR_SOURCE)

$(BONJOUR_DIR)/.unpacked: $(DL_DIR)/$(BONJOUR_SOURCE)
	zcat $(DL_DIR)/$(BONJOUR_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	toolchain/patch-kernel.sh $(BONJOUR_DIR) package/bonjour mDNS*.patch
	touch $(BONJOUR_DIR)/.unpacked

$(BONJOUR_DIR)/mDNSPosix/build/prod/mDNSResponderPosix: $(BONJOUR_DIR)/.unpacked
	$(MAKE) $(JLEVEL) CC="$(TARGET_CROSS)gcc" CFLAGS_DEBUG="$(TARGET_CFLAGS) -DMDNS_DEBUGMSGS=0" os=linux -C $(BONJOUR_DIR)/mDNSPosix SAResponder

$(TARGET_DIR)/usr/sbin/mDNSResponder: $(BONJOUR_DIR)/mDNSPosix/build/prod/mDNSResponderPosix
	mkdir -p $(TARGET_DIR)/usr/sbin
	install -m 0755 $(BONJOUR_DIR)/mDNSPosix/build/prod/mDNSResponderPosix $(TARGET_DIR)/usr/sbin/mDNSResponder
	$(STRIP) $(TARGET_DIR)/usr/sbin/mDNSResponder
	install -m 0755 package/bonjour/S50bonjour $(TARGET_DIR)/etc/init.d
	install -m 0644 package/bonjour/bonjour.conf $(TARGET_DIR)/etc

bonjour: uclibc $(TARGET_DIR)/usr/sbin/mDNSResponder

bonjour-clean:
	rm -f $(TARGET_DIR)/usr/sbin/mDNSResponder $(TARGET_DIR)/etc/init.d/S50bonjour $(TARGET_DIR)/etc/bonjour.conf
	-$(MAKE) $(JLEVEL) -C $(BONJOUR_DIR)/mDNSPosix clean

bonjour-dirclean:
	rm -rf $(BONJOUR_DIR)
#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(strip $(BR2_PACKAGE_BONJOUR)),y)
TARGETS+=bonjour
endif
